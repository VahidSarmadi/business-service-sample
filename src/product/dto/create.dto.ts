import { BaseProductDto } from "./base.dto";
import { ApiProperty } from "@nestjs/swagger";
import {IsArray, IsEnum, IsMongoId, IsNotEmpty, IsOptional, IsString} from 'class-validator';
import {ProductStatus} from "../schemas/product.schema";


export class CreateProductDto extends BaseProductDto {
    @ApiProperty()
    @IsString()
    @IsMongoId()
    @IsNotEmpty()
    venueID: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    title: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    description: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    category: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    price: string;

    @ApiProperty()
    @IsArray()
    @IsNotEmpty()
    photos: string[];

    @ApiProperty({ enum: ProductStatus })
    @IsEnum(ProductStatus)
    @IsNotEmpty()
    status: ProductStatus;
}