import { BaseProductDto } from './base.dto';
import { ApiProperty } from "@nestjs/swagger";
import {IsArray, IsEnum, IsMongoId, IsNotEmpty, IsOptional, IsString} from "class-validator";

enum ProductStatus {
    Draft,
    Active,
    Archived
}

export class UpdateProductDto extends BaseProductDto {
    @ApiProperty()
    @IsString()
    @IsMongoId()
    @IsNotEmpty()
    venueID: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    title: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    description: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    category: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    price: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    @IsEnum(ProductStatus)
    status: string;

    @ApiProperty()
    @IsArray()
    @IsNotEmpty()
    photos: string[];
}