import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
    UseInterceptors,
    UploadedFile,
    UseGuards
} from '@nestjs/common';
import {ProductService} from "./product.service";
import {CreateProductDto} from "./dto/create.dto";
import {UpdateProductDto} from "./dto/update.dto";
import {ApiTags} from "@nestjs/swagger";
import {AuthGuard, BusinessAuthGuard} from "../account/auth.guard";
import {RoleGuard} from "../account/role.guard";

@Controller('product')
@ApiTags('product')
export class ProductController {
    constructor(private readonly service: ProductService) {}

    @Get()
    @UseGuards(AuthGuard, RoleGuard)
    async getAll() {
        const data = await this.service.getAll();
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get(':id')
    @UseGuards(AuthGuard, RoleGuard)
    async getOne(@Param('id') id: string) {
        const data = await this.service.getOne(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/venue/:id')
    @UseGuards(AuthGuard, RoleGuard)
    async getByVenueID(@Param('id') id: string) {
        const data = await this.service.getByVenueID(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/venue/:id/categorized')
    // @UseGuards(AuthGuard, RoleGuard)
    async getByVenueID_categorized(@Param('id') id: string) {
        const data = await this.service.getByVenueID_categorized(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/venue/:id/draft')
    @UseGuards(AuthGuard, RoleGuard)
    async getByVenueID_draft(@Param('id') id: string) {
        const data = await this.service.getByVenueID_draft(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/venue/:id/active')
    @UseGuards(AuthGuard, RoleGuard)
    async getByVenueID_active(@Param('id') id: string) {
        const data = await this.service.getByVenueID_active(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/venue/:id/archived')
    @UseGuards(AuthGuard, RoleGuard)
    async getByVenueID_archived(@Param('id') id: string) {
        const data = await this.service.getByVenueID_archived(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/category/:category')
    @UseGuards(AuthGuard, RoleGuard)
    async getByCategory(@Param('category') category: string) {
        const data = await this.service.getByCategory(category);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Post()
    @UseGuards(AuthGuard, RoleGuard)
    async create(@Body() createProductDto: CreateProductDto) {
        const newProduct = await this.service.create(createProductDto);
        return {
            statusCode: 200,
            message: 'Product has been submitted successfully!',
            data: newProduct
        };
    }

    @Put(':id')
    @UseGuards(AuthGuard, RoleGuard)
    async update(@Param('id') id: string, @Body() updateProductDto: UpdateProductDto) {
        const updatedProduct = await this.service.update(id, updateProductDto);
        return {
            statusCode: 200,
            message: 'Product has been updated successfully!',
            data: updatedProduct
        };
    }

    @Delete(':id')
    @UseGuards(AuthGuard, RoleGuard)
    async delete(@Param('id') id: string) {
        const deletedProduct = await this.service.delete(id);
        return {
            statusCode: 200,
            message: 'Product has been deleted!',
            data: deletedProduct
        };
    }

    @Delete('/permanently/:id')
    @UseGuards(AuthGuard, RoleGuard)
    async deletePermanently(@Param('id') id: string) {
        const deletedProduct = await this.service.deletePermanently(id);
        return {
            statusCode: 200,
            message: 'Product has been deleted permanently!',
            data: deletedProduct
        };
    }
}
