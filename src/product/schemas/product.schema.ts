import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export enum ProductStatus {
    Draft = "Draft",
    Active = "Active",
    Archived = "Archived"
}

export type ProductDocument = Product & Document;

@Schema({ timestamps: true })
export class Product {
    // Venue ID
    @Prop({ required: true })
    venueID: string;

    @Prop({ required: true })
    title: string;

    @Prop({ required: true })
    description: string;

    @Prop({ required: true })
    category: string;

    @Prop({ required: true })
    price: string;

    @Prop({ required: true, type: [String] })
    photos: string[];

    @Prop({ type: [String] })
    tags: string[];

    @Prop({ type: String, enum: ProductStatus, default: ProductStatus.Active})
    status: string;

    @Prop()
    deletedAt?: Date;

    @Prop()
    createdBy?: string;

    @Prop()
    updatedBy?: string;

    @Prop({ default: false })
    deleted: boolean
}

export const ProductSchema = SchemaFactory.createForClass(Product);