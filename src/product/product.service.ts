import { Injectable } from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Product, ProductDocument, ProductStatus} from "./schemas/product.schema";
import {Model} from "mongoose";
import {CreateProductDto} from "./dto/create.dto";
import {UpdateProductDto} from "./dto/update.dto";

@Injectable()
export class ProductService {
    constructor(
        @InjectModel(Product.name) private readonly productModel: Model<ProductDocument>,
    ) {}

    async getAll(): Promise<Product[]> {
        return await this.productModel.find({ 'deleted': false }).exec();
    }

    async getOne(id: string): Promise<Product> {
        return await this.productModel.findOne({'_id': id, 'deleted': false}).exec();
    }

    async getByVenueID(id: string): Promise<Product[]> {
        return await this.productModel.find({ 'venueID': id, 'deleted': false }).exec();
    }

    async getByVenueID_categorized(id: string): Promise<{category: string, data: Product[]}[]> {
        const products = await this.productModel.aggregate([
            { $match: { venueID: id, deleted: false } },
            { $group: { _id: "$category", products: { $push: "$$ROOT" } } }
        ]);

        // const result = {};
        //
        // for (const product of products) {
        //     result[product._id] = product.products;
        // }
        //
        // return result;
        return products.map(({_id, products}) => ({ category: _id, data: products }));

    }

    async getByVenueID_draft(id: string): Promise<Product[]> {
        return await this.productModel.find({ 'venueID': id, 'deleted': false, status: ProductStatus.Draft }).exec();
    }

    async getByVenueID_active(id: string): Promise<Product[]> {
        return await this.productModel.find({ 'venueID': id, 'deleted': false, status: ProductStatus.Active }).exec();
    }

    async getByVenueID_archived(id: string): Promise<Product[]> {
        return await this.productModel.find({ 'venueID': id, 'deleted': false, status: ProductStatus.Archived }).exec();
    }

    async getByCategory(category: string): Promise<Product[]> {
        return await this.productModel.find({ 'category': category, 'deleted': false }).exec();
    }

    async create(createProductDto: CreateProductDto, userID?: string): Promise<Product> {
        return await new this.productModel({
            ...createProductDto,
            createdBy: userID || undefined
        }).save();
    }

    async update(id: string, updateProductDto: UpdateProductDto, userID?: string): Promise<Product> {
        return await this.productModel.findByIdAndUpdate(id, {
            ...updateProductDto,
            updatedBy: userID || undefined
        }).exec();
    }

    async delete(id: string): Promise<Product> {
        return await this.productModel.findByIdAndUpdate(id,{
            deleted: true,
            deletedAt: new Date()
        }).exec();
    }

    async deletePermanently(id: string): Promise<Product> {
        return await this.productModel.findByIdAndDelete(id).exec();
    }
}
