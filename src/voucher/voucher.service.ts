import {forwardRef, Inject, Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Voucher, VoucherDocument} from "./schemas/voucher.schema";
import {Model} from "mongoose";
import {OrderService} from "../order/order.service";

@Injectable()
export class VoucherService {
    constructor(
        @InjectModel(Voucher.name)
        private readonly voucherModel: Model<VoucherDocument>,

        @Inject(forwardRef(() => OrderService))
        private readonly orderService: OrderService,
    ) {}

    async getAll(): Promise<Voucher[]> {
        return await this.voucherModel.find({ 'deleted': false }).exec();
    }

    async getOne(id: string): Promise<Voucher> {
        return await this.voucherModel.findOne({'_id': id, 'deleted': false, 'used': false}).populate('product').exec();
    }

    async getByVenueID(id: string): Promise<Voucher[]> {
        return await this.voucherModel.find({ 'venue': id, 'deleted': false }).populate('product').exec();
    }

    async getByProductID(id: string): Promise<Voucher[]> {
        return await this.voucherModel.find({ 'product': id, 'deleted': false }).populate('product').exec();
    }

    async getByUserID(id: string): Promise<Voucher[]> {
        return await this.voucherModel.find({ 'userID': id, 'deleted': false }).populate('product').exec();
    }

    async getForActiveOrder(userID: string): Promise<Voucher[]> {
        const order = await this.orderService.getLastActiveOrderByUserID(userID);

        if(!order)  return [];

        let voucherList = [];

        for(let i = 0; i < order.items.length; i++) {
            let vouchers = await this.voucherModel.find({ 'userID': userID, venue: order.venueID, product: order.items[i].productID, 'used': false, 'deleted': false }).populate('product').exec();
            voucherList.push(...vouchers);
        }
        return voucherList;
    }

    async create(createVoucherDto: object, userID?: string): Promise<Voucher> {
        return await new this.voucherModel({
            ...createVoucherDto
        }).save();
    }

    async delete(id: string): Promise<Voucher> {
        return await this.voucherModel.findByIdAndUpdate(id,{
            deleted: true,
            deletedAt: new Date()
        }).exec();
    }

    async deletePermanently(id: string): Promise<Voucher> {
        return await this.voucherModel.findByIdAndDelete(id).exec();
    }

    async generateFake(): Promise<Voucher> {
        return await new this.voucherModel({
            venue: '64071f9de2bd0b457ff60d3f',
            product: '642042088ec114fa0d8ae842',
            userID: '1664'
        }).save();
    }

    async generateFake2(): Promise<Voucher> {
        return await new this.voucherModel({
            venue: '64071f9de2bd0b457ff60d3f',
            product: '642042088ec114fa0d8ae842',
            userID: '1556'
        }).save();
    }


}
