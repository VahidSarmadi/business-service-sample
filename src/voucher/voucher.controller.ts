import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
    UseInterceptors,
    UploadedFile,
    UseGuards, Req
} from '@nestjs/common';
import {VoucherService} from "./voucher.service";
import {CreateVoucherDto} from "./dto/create.dto";
import {UpdateVoucherDto} from "./dto/update.dto";
import {ApiTags} from "@nestjs/swagger";
import {AuthGuard, BusinessAuthGuard} from "../account/auth.guard";
import {RoleGuard} from "../account/role.guard";

@Controller('voucher')
@ApiTags('voucher')
export class VoucherController {
    constructor(private readonly service: VoucherService) {}

    @Get('/generateFake')
    @UseGuards(AuthGuard, RoleGuard)
    async getAll() {
        const data = await this.service.generateFake();
        const data2 = await this.service.generateFake2();
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get(':id')
    @UseGuards(AuthGuard, RoleGuard)
    async getOne(@Param('id') id: string) {
        const data = await this.service.getOne(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/venue/:venueID')
    @UseGuards(AuthGuard, RoleGuard)
    async getByVenueID(@Param('venueID') id: string) {
        const data = await this.service.getByVenueID(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/user/:userID')
    @UseGuards(AuthGuard, RoleGuard)
    async getByUserID(@Param('userID') id: string) {
        const data = await this.service.getByUserID(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/current/user')
    @UseGuards(AuthGuard, RoleGuard)
    async getByToken(@Req() req) {
        const data = await this.service.getByUserID(req.user.id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/current/user/active-order')
    @UseGuards(AuthGuard, RoleGuard)
    async getForActiveOrder(@Req() req) {
        const data = await this.service.getForActiveOrder(req.user.id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Delete(':id')
    @UseGuards(AuthGuard, RoleGuard)
    async delete(@Param('id') id: string) {
        const deletedVoucher = await this.service.delete(id);
        return {
            statusCode: 200,
            message: 'Voucher has been deleted!',
            data: deletedVoucher
        };
    }

    @Delete('/permanently/:id')
    @UseGuards(AuthGuard, RoleGuard)
    async deletePermanently(@Param('id') id: string) {
        const deletedVoucher = await this.service.deletePermanently(id);
        return {
            statusCode: 200,
            message: 'Voucher has been deleted permanently!',
            data: deletedVoucher
        };
    }
}
