import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from "mongoose";
import {Product} from "../../product/schemas/product.schema";
import {Profile} from "../../profile/schemas/profile.schema";

export type VoucherDocument = Voucher & Document;

@Schema({ timestamps: true })
export class Voucher {
    @Prop()
    code?: string;

    @Prop()
    amount?: number;

    @Prop({ default: () => Date.now() + 30 * 24 * 60 * 60 * 1000 }) // Set default to 1 month from now
    expiryDate?: Date;

    @Prop({type: mongoose.Schema.Types.ObjectId, ref: 'Product'})
    product: Product;

    @Prop({type: mongoose.Schema.Types.ObjectId, ref: 'Profile'})
    venue: Profile;

    @Prop({ required: true })
    userID: string;

    @Prop({ default: false })
    used: boolean;

    @Prop()
    usedAt?: Date;

    @Prop()
    deletedAt?: Date;

    @Prop()
    createdBy?: string;

    @Prop()
    updatedBy?: string;

    @Prop({ default: false })
    deleted: boolean;
}

export const VoucherSchema = SchemaFactory.createForClass(Voucher);