import {forwardRef, Module} from '@nestjs/common';
import { VoucherService } from './voucher.service';
import { VoucherController } from './voucher.controller';
import {ConfigModule} from "@nestjs/config";
import {HttpModule} from "@nestjs/axios";
import {MongooseModule} from "@nestjs/mongoose";
import {Voucher, VoucherSchema} from "./schemas/voucher.schema";
import {OrderModule} from "../order/order.module";
import {OrderService} from "../order/order.service";

@Module({
  imports: [
    ConfigModule,
    HttpModule,
    MongooseModule.forFeature([{ name: Voucher.name, schema: VoucherSchema }]),
    forwardRef(() => OrderModule)
  ],
  providers: [VoucherService],
  controllers: [VoucherController],
  exports: [VoucherService]
})
export class VoucherModule {}
