import { Module } from '@nestjs/common';
import {ConfigModule, ConfigService} from '@nestjs/config';
import { MongooseModule } from "@nestjs/mongoose";
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {applicationConfig, databaseConfig, servicesConfig, validationSchema} from './config';
import { BusinessCategoryModule } from './business-category/business-category.module';
import { ProductModule } from './product/product.module';
import {OrderModule} from "./order/order.module";
import {AccountModule} from "./account/account.module";
import {ProfileModule} from "./profile/profile.module";
import {VoucherModule} from "./voucher/voucher.module";

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: process.env.NODE_ENV
          ? `.env.${process.env.NODE_ENV}`
          : '.env',
      isGlobal: true,
      cache: true,
      expandVariables: true,
      validationSchema,
      load: [applicationConfig, databaseConfig, servicesConfig],
      validationOptions: {
        allowUnknown: true,
      },
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => ({
        uri: config.get('database.uri'),
        user: config.get('database.username'),
        pass: config.get('database.password'),
        dbName: config.get('database.name'),
        useNewUrlParser: true,
        useUnifiedTopology: true,
      }),
      inject: [ConfigService],
    }),
    AccountModule,
    BusinessCategoryModule,
    ProfileModule,
    ProductModule,
    OrderModule,
    VoucherModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
