import { BaseBusinessCategoryDto } from "./base.dto";
import { ApiProperty } from "@nestjs/swagger";
import { IsArray, IsNotEmpty } from 'class-validator';


export class UpdateSubcategoryDto extends BaseBusinessCategoryDto {
    @ApiProperty()
    @IsNotEmpty()
    @IsArray()
    subcategories: string[];
}