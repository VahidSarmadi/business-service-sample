import { BaseBusinessCategoryDto } from "./base.dto";
import { ApiProperty } from "@nestjs/swagger";
import { IsArray, IsNotEmpty, IsOptional, IsString } from 'class-validator';


export class CreateBusinessCategoryDto extends BaseBusinessCategoryDto {
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    name: string;

    @ApiProperty()
    @IsOptional()
    @IsArray()
    subcategories?: string[];

    @ApiProperty()
    @IsOptional()
    @IsArray()
    atmosphereList?: string[];
}