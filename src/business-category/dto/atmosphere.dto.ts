import { BaseBusinessCategoryDto } from "./base.dto";
import { ApiProperty } from "@nestjs/swagger";
import { IsArray, IsNotEmpty } from 'class-validator';


export class UpdateAtmosphereDto extends BaseBusinessCategoryDto {
    @ApiProperty()
    @IsNotEmpty()
    @IsArray()
    atmosphereList: string[];
}