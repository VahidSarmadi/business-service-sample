import { Injectable } from '@nestjs/common';
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";
import { BusinessCategory, BusinessCategoryDocument } from "./schemas/business-category.schema";
import { CreateBusinessCategoryDto } from "./dto/create.dto";
import { UpdateBusinessCategoryDto } from "./dto/update.dto";
import { UpdateSubcategoryDto } from "./dto/subcategory.dto";
import {UpdateAtmosphereDto} from "./dto/atmosphere.dto";

@Injectable()
export class BusinessCategoryService {
    constructor(
        @InjectModel(BusinessCategory.name) private readonly categoryModel: Model<BusinessCategoryDocument>,
    ) {}

    async getAll(): Promise<BusinessCategory[]> {
        return await this.categoryModel.find({ 'deleted': false }).exec();
    }

    async getOne(id: string): Promise<BusinessCategory> {
        return await this.categoryModel.findOne({'_id': id, 'deleted': false}).exec();
    }

    async getOneByName(name: string): Promise<BusinessCategory> {
        return await this.categoryModel.findOne({'name': name, 'deleted': false}).exec();
    }

    async getSubcategories(id: string): Promise<Object> {
        const category = await this.categoryModel.findOne({'_id': id, 'deleted': false}).exec();
        return { subcategories: category.subcategories };
    }

    async getAtmosphereList(id: string): Promise<Object> {
        const category = await this.categoryModel.findOne({'_id': id, 'deleted': false}).exec();
        return { atmosphereList: category.atmosphereList };
    }

    async create(createCategoryDto: CreateBusinessCategoryDto, userID?: string): Promise<BusinessCategory> {
        return await new this.categoryModel({
            ...createCategoryDto,
            createdBy: userID || undefined
        }).save();
    }

    async addSubcategory(id: string, updateSubcategoryDto: UpdateSubcategoryDto, userID?: string): Promise<BusinessCategory> {
        return await this.categoryModel.findByIdAndUpdate(id, {
            ...updateSubcategoryDto,
            updatedBy: userID || undefined
        }).exec();
    }

    async addAtmosphere(id: string, updateAtmosphereDto: UpdateAtmosphereDto, userID?: string): Promise<BusinessCategory> {
        return await this.categoryModel.findByIdAndUpdate(id, {
            ...updateAtmosphereDto,
            updatedBy: userID || undefined
        }).exec();
    }

    async update(id: string, updateCategoryDto: UpdateBusinessCategoryDto, userID?: string): Promise<BusinessCategory> {
        return await this.categoryModel.findByIdAndUpdate(id, {
            ...updateCategoryDto,
            updatedBy: userID || undefined
        }).exec();
    }

    async delete(id: string): Promise<BusinessCategory> {
        return await this.categoryModel.findByIdAndUpdate(id,{
            deleted: true,
            deletedAt: new Date()
        }).exec();
    }

    async deletePermanently(id: string): Promise<BusinessCategory> {
        return await this.categoryModel.findByIdAndDelete(id).exec();
    }
}
