import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type BusinessCategoryDocument = BusinessCategory & Document;

@Schema({ timestamps: true })
export class BusinessCategory {
    @Prop({ required: true })
    name: string;

    @Prop({ type: [String] })
    subcategories: string[];

    @Prop({ type: [String] })
    atmosphereList: string[];

    @Prop()
    deletedAt?: Date;

    @Prop()
    createdBy?: string;

    @Prop()
    updatedBy?: string;

    @Prop({ default: false })
    deleted: boolean
}

export const BusinessCategorySchema = SchemaFactory.createForClass(BusinessCategory);