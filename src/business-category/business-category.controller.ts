import {Body, Controller, Delete, Get, Param, Post, Put, UseGuards} from '@nestjs/common';
import {BusinessCategoryService} from "./business-category.service";
import {AuthGuard, BusinessAuthGuard} from "../account/auth.guard";
import {RoleGuard} from "../account/role.guard";
import {Roles} from "../account/roles.decorator";
import {CreateBusinessCategoryDto} from "./dto/create.dto";
import {UpdateBusinessCategoryDto} from "./dto/update.dto";
import {UpdateSubcategoryDto} from "./dto/subcategory.dto";
import {ApiOperation, ApiTags} from "@nestjs/swagger";
import {UpdateAtmosphereDto} from "./dto/atmosphere.dto";

@Controller('business-category')
@ApiTags('business-category')
export class BusinessCategoryController {
    constructor(private readonly service: BusinessCategoryService) {}

    @Get()
    @UseGuards(AuthGuard, RoleGuard)
    async getAll() {
        const data = await this.service.getAll();
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get(':id')
    @UseGuards(AuthGuard, RoleGuard)
    async getOne(@Param('id') id: string) {
        const data = await this.service.getOne(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get(':name')
    @UseGuards(AuthGuard, RoleGuard)
    async getOneByName(@Param('name') name: string) {
        const data = await this.service.getOneByName(name);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get(':id/subcategories')
    @UseGuards(AuthGuard, RoleGuard)
    async getSubcategories(@Param('id') id: string) {
        const data = await this.service.getSubcategories(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get(':id/atmosphereList')
    @UseGuards(AuthGuard, RoleGuard)
    async getAtmosphere(@Param('id') id: string) {
        const data = await this.service.getAtmosphereList(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Post()
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async create(@Body() createCategoryDto: CreateBusinessCategoryDto) {
        const newCategory = await this.service.create(createCategoryDto);
        return {
            statusCode: 200,
            message: 'Category has been submitted successfully!',
            data: newCategory
        };
    }

    @Post('/subcategory/:id')
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async addSubcategory(@Param('id') id: string,@Body() updateSubcategoryDto: UpdateSubcategoryDto) {
        const newCategory = await this.service.addSubcategory(id, updateSubcategoryDto);
        return {
            statusCode: 200,
            message: 'Subcategory has been submitted successfully!',
            data: newCategory
        };
    }

    @Post('/atmosphere/:id')
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async addAtmosphere(@Param('id') id: string,@Body() updateAtmosphereDto: UpdateAtmosphereDto) {
        const newCategory = await this.service.addAtmosphere(id, updateAtmosphereDto);
        return {
            statusCode: 200,
            message: 'Atmosphere has been submitted successfully!',
            data: newCategory
        };
    }

    @Put(':id')
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async update(@Param('id') id: string, @Body() updateCategoryDto: UpdateBusinessCategoryDto) {
        const updatedCategory = await this.service.update(id, updateCategoryDto);
        return {
            statusCode: 200,
            message: 'Category has been updated successfully!',
            data: updatedCategory
        };
    }

    @Delete(':id')
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async delete(@Param('id') id: string) {
        const deletedCategory = await this.service.delete(id);
        return {
            statusCode: 200,
            message: 'Category has been deleted!',
            data: deletedCategory
        };
    }

    @Delete('/permanently/:id')
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async deletePermanently(@Param('id') id: string) {
        const deletedCategory = await this.service.deletePermanently(id);
        return {
            statusCode: 200,
            message: 'Category has been deleted permanently!',
            data: deletedCategory
        };
    }
}
