import { Module } from '@nestjs/common';
import { BusinessCategoryService } from './business-category.service';
import { BusinessCategoryController } from './business-category.controller';
import { ConfigModule } from "@nestjs/config";
import { HttpModule } from "@nestjs/axios";
import {MongooseModule} from "@nestjs/mongoose";
import {BusinessCategory, BusinessCategorySchema} from "./schemas/business-category.schema";

@Module({
  imports: [
    ConfigModule,
    HttpModule,
    MongooseModule.forFeature([
      { name: BusinessCategory.name, schema: BusinessCategorySchema }
    ]),
  ],
  providers: [BusinessCategoryService],
  controllers: [BusinessCategoryController],
})
export class BusinessCategoryModule {}
