import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
    UseInterceptors,
    UploadedFile,
    UseGuards, Req, NotFoundException, Query, Inject, forwardRef
} from '@nestjs/common';
import {TransactionService} from "./transaction.service";
import {ApiTags} from "@nestjs/swagger";
import {AuthGuard, BusinessAuthGuard} from "../account/auth.guard";
import {RoleGuard} from "../account/role.guard";
import {OrderService} from "../order/order.service";
import {OrderStatus, OrderType} from "../order/schemas/order.schema";
import {TransactionStatus} from "./schemas/transaction.schema";

@Controller('transaction')
@ApiTags('transaction')
export class TransactionController {
    constructor(
        private readonly service: TransactionService,

        @Inject(forwardRef(() => OrderService))
        private readonly orderService: OrderService,
    ) {}

    @Get('/')
    @UseGuards(AuthGuard, RoleGuard)
    async getAll() {
        const data = await this.service.getAll();
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get(':id')
    @UseGuards(AuthGuard, RoleGuard)
    async getOne(@Param('id') id: string) {
        const data = await this.service.getOne(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/venue/:venueID')
    @UseGuards(AuthGuard, RoleGuard)
    async getByVenueID(@Param('venueID') id: string) {
        const data = await this.service.getByVenueID(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/order/:orderID')
    @UseGuards(AuthGuard, RoleGuard)
    async getByOrderID(@Param('orderID') id: string) {
        const data = await this.service.getByVenueID(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/user/:userID')
    @UseGuards(AuthGuard, RoleGuard)
    async getByUserID(@Param('userID') id: string) {
        const data = await this.service.getByUserID(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/current/user')
    @UseGuards(AuthGuard, RoleGuard)
    async getByToken(@Req() req) {
        const data = await this.service.getByUserID(req.user.id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/payment-callback/:id')
    async paymentCallback(@Query('status') status: string, @Param('id') transactionId: string): Promise<string> {
        // Find the transaction by ID
        const transaction = await this.service.getOne(transactionId);
        if (!transaction) {
            throw new NotFoundException('Transaction not found!');
        }

        if (status === "1") {
            transaction.status = TransactionStatus.Completed;
            const order = await this.orderService.confirmPayment(transaction.order.toString(), transaction.amount);

            // @ts-ignore
            await transaction.save();
            return "Payment is completed successfully";
        } else {
            transaction.status = TransactionStatus.Failed;
            // @ts-ignore
            await transaction.save();
            return "Payment failed";
        }

        // Update the transaction status and payment data
        // transaction.status = TransactionStatus.Paid;
        // transaction.paymentData = {
        //     // Add payment data received from the third-party payment API
        // };
        // await transaction.save();
        // // Update the order status
        // await this.orderService.updateOrderStatus(transaction.orderID, OrderStatus.Confirmed);
        // // Redirect the user to the order confirmation page
        // return res.redirect(`/order/${transaction.orderID}/confirmation`);
    }

    @Delete(':id')
    @UseGuards(AuthGuard, RoleGuard)
    async delete(@Param('id') id: string) {
        const deletedVoucher = await this.service.delete(id);
        return {
            statusCode: 200,
            message: 'Voucher has been deleted!',
            data: deletedVoucher
        };
    }

    @Delete('/permanently/:id')
    @UseGuards(AuthGuard, RoleGuard)
    async deletePermanently(@Param('id') id: string) {
        const deletedVoucher = await this.service.deletePermanently(id);
        return {
            statusCode: 200,
            message: 'Voucher has been deleted permanently!',
            data: deletedVoucher
        };
    }
}
