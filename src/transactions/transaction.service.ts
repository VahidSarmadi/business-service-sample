import {BadRequestException, forwardRef, Inject, Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {OrderService} from "../order/order.service";
import {Transaction, TransactionDocument} from "./schemas/transaction.schema";
import {CreateOrderDto} from "../order/dto/create.dto";
import {CreateTransactionDto} from "./dto/create.dto";
import {HttpService} from "@nestjs/axios";
import {ConfigService} from "@nestjs/config";

@Injectable()
export class TransactionService {
    constructor(
        @InjectModel(Transaction.name)
        private readonly transactionModel: Model<TransactionDocument>,

        @Inject(forwardRef(() => OrderService))
        private readonly orderService: OrderService,

        private readonly httpService: HttpService,
        private configService: ConfigService
    ) {}

    async getAll(): Promise<Transaction[]> {
        return await this.transactionModel.find({ 'deleted': false }).exec();
    }

    async getOne(id: string): Promise<Transaction> {
        return await this.transactionModel.findOne({'_id': id, 'deleted': false}).exec();
    }

    async getByVenueID(id: string): Promise<Transaction[]> {
        return await this.transactionModel.find({ 'venue': id, 'deleted': false }).exec();
    }

    async getByOrderID(id: string): Promise<Transaction[]> {
        return await this.transactionModel.find({ 'order': id, 'deleted': false }).exec();
    }

    async getByUserID(id: string): Promise<Transaction[]> {
        return await this.transactionModel.find({ 'userID': id, 'deleted': false }).exec();
    }

    async requestPaymentGateway(transaction: Transaction): Promise<{ error, checkoutURL }> {

        let data = {
            user_id: transaction.userInfo.id || 'Guest',
            method: transaction.method || 'STRIPE',
            service_name: 'Business',
            currency: 'USD',
            amount: transaction.amount,
            callback_url: process.env.DOMAIN + '/transaction/payment-callback/' + transaction['_id'],
            description: transaction.description
        }
        try {
            let response = await this.httpService.axiosRef.post(
                `${this.configService.get('services.userManagement.testUrl')}${this.configService.get('services.userManagement.endpoints.paymentGateway')}`,
                data,
                {headers: { 'x-api-key': 'g@9j^P7WmCf2&5Kx'}}
            );
            console.log(response.data);
            if(!response.data.checkout_url) return {'error': response.data.message, 'checkoutURL': null};

            transaction.paymentId = response.data.transaction.id;
            transaction.checkoutURL = response.data.checkout_url;
            // @ts-ignore
            await transaction.save();

            return {error: null, checkoutURL: response.data.checkout_url};
        } catch (e) {
            console.error(e);
            throw new BadRequestException(`third party service has error!`);
        }
    }

    async create(createTransactionDto: CreateTransactionDto, userID?: string): Promise<Transaction> {
        console.log(createTransactionDto);
        return await new this.transactionModel({
            ...createTransactionDto
        }).save();
    }

    async delete(id: string): Promise<Transaction> {
        return await this.transactionModel.findByIdAndUpdate(id,{
            deleted: true,
            deletedAt: new Date()
        }).exec();
    }

    async deletePermanently(id: string): Promise<Transaction> {
        return await this.transactionModel.findByIdAndDelete(id).exec();
    }



}
