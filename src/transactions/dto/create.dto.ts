import {
    IsArray,
    IsEnum,
    IsMongoId,
    IsNotEmpty,
    IsNumber,
    IsOptional,
    IsString,
    ValidateIf,
    ValidateNested
} from 'class-validator';
import {ApiProperty} from '@nestjs/swagger';
import {Type} from 'class-transformer';



class UserInfoDto {
    @ApiProperty({ description: 'Mobile number' })
    @IsNotEmpty()
    @IsString()
    readonly mobile?: string;

    @ApiProperty({ description: 'Name of the user' })
    @IsNotEmpty()
    @IsString()
    readonly name?: string;

    @ApiProperty({ description: 'userID' })
    @IsNotEmpty()
    @IsString()
    readonly id?: string;
}

export class CreateTransactionDto {
    @ApiProperty({ description: 'Amount' })
    @IsNotEmpty()
    @IsNumber()
    readonly amount: number;

    @ApiProperty({ description: 'Order ID' })
    @IsNotEmpty()
    @IsString()
    @IsMongoId()
    readonly orderID: string;

    @ApiProperty({ description: 'Venue ID' })
    @IsNotEmpty()
    @IsString()
    @IsMongoId()
    readonly venueID: string;

    @ApiProperty({ description: 'User info' })
    @ValidateNested()
    @Type(() => UserInfoDto)
    readonly userInfo: UserInfoDto;

    readonly method?: string;

    readonly description?: string;

    readonly currency?: string;
}