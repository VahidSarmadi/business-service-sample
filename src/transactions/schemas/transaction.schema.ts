import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from "mongoose";
import {Order} from "../../order/schemas/order.schema";
import {Profile} from "../../profile/schemas/profile.schema";

export enum TransactionStatus {
    Pending = "Pending",
    Processing = "Processing",
    Completed = "Completed",
    Failed = "Failed",
    Canceled = "Canceled",
    Refunded = "Refunded",
}
export type TransactionDocument = Transaction & Document;

@Schema({ timestamps: true })
export class Transaction {
    @Prop()
    paymentId?: string;

    @Prop()
    checkoutURL?: string;

    @Prop()
    description?: string;

    @Prop({default: 'STRIPE'})
    method: string;

    @Prop()
    amount: number;

    @Prop({type: mongoose.Schema.Types.ObjectId, ref: 'Order'})
    order: Order;

    @Prop({type: mongoose.Schema.Types.ObjectId, ref: 'Profile'})
    venue: Profile;

    @Prop({ type: Object })
    userInfo: {
        id?: string;
        name: string;
        mobile: string;
    }

    @Prop({ type: String, enum: TransactionStatus, default: TransactionStatus.Pending})
    status: string;

    @Prop()
    deletedAt?: Date;

    @Prop()
    createdBy?: string;

    @Prop()
    updatedBy?: string;

    @Prop({ default: false })
    deleted: boolean;
}

export const TransactionSchema = SchemaFactory.createForClass(Transaction);