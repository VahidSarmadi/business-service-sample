import * as Joi from 'joi';
import applicationConfig from './application.config';
import databaseConfig from './database.config';
import servicesConfig from './services.config';

export const validationSchema = Joi.object({
    application:{
        port: Joi.number().default(3000),
    },
    database: {
        uri: Joi.string().required(),
        name: Joi.string().required(),
    },
});

export { applicationConfig, databaseConfig, servicesConfig };