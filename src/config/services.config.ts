import { registerAs } from '@nestjs/config';

export default registerAs('services', () => ({
    userManagement: {
        baseUrl: process.env.USER_MANAGEMENT_URL,
        testUrl: process.env.USER_MANAGEMENT_TEST_URL,
        endpoints: {
            getID: '/api/v1/users/get-id', // authorization = true,
            paymentGateway: '/api/v1/payment/general-transactions/'
        }
    },
}));