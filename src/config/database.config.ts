import { registerAs } from '@nestjs/config';

export default registerAs('database', () => ({
    uri: process.env.DATABASE_URL,
    name: process.env.DATABASE_NAME,
    username: process.env.DATABASE_USERNAME || '',
    password: process.env.DATABASE_PASSWORD || '',
}));