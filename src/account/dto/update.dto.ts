import { BaseAccountDto } from './base.dto';
import { ApiProperty } from "@nestjs/swagger";
import {IsMongoId, IsNotEmpty, IsOptional, IsString} from "class-validator";

export class UpdateAccountDto extends BaseAccountDto {
    @ApiProperty()
    @IsString()
    @IsOptional()
    label?: string;

    @ApiProperty()
    @IsString()
    @IsOptional()
    description?: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    // @IsMongoId()
    role: string;
}