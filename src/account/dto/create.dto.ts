import { BaseAccountDto } from "./base.dto";
import { ApiProperty } from "@nestjs/swagger";
import {IsMongoId, IsNotEmpty, IsOptional, IsString} from 'class-validator';


export class CreateAccountDto extends BaseAccountDto {
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    userID: string;

    //todo: uncomment IsMongoId()
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    // @IsMongoId()
    venueID: string;

    @ApiProperty()
    @IsString()
    @IsOptional()
    label?: string;

    @ApiProperty()
    @IsString()
    @IsOptional()
    description?: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    // @IsMongoId()
    role: string;
}