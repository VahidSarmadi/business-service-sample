import {Body, Controller, Delete, Get, Param, Post, Put, Req, UseGuards} from '@nestjs/common';
import {AccountService} from "./account.service";
import {AuthGuard, BusinessAuthGuard} from "./auth.guard";
import {RoleGuard} from "./role.guard";
import {Roles} from "./roles.decorator";
import {CreateAccountDto} from "./dto/create.dto";
import {UpdateAccountDto} from "./dto/update.dto";
import {ApiOperation, ApiTags} from "@nestjs/swagger";

@Controller('account')
@ApiTags('account')
export class AccountController {
    constructor(private readonly service: AccountService) {}

    @Get()
    @ApiOperation({ summary: 'Get all accounts.' })
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async getAll() {
        const data = await this.service.getAll();
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/id/:id')
    @ApiOperation({ summary: 'Get specific account by its "id".' })
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async getByID(@Param('id') id: string) {
        const data = await this.service.getOne(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/user')
    @ApiOperation({ summary: 'Get current user account list.' })
    @UseGuards(AuthGuard, RoleGuard)
    async getByToken(@Req() req) {
        const data = await this.service.getByUserID(req.user.id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/user/:id')
    @ApiOperation({ summary: 'Get some user account list by "userID".' })
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async getByUserID(@Param('id') id: string) {
        const data = await this.service.getByUserID(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/venue/:id')
    @ApiOperation({ summary: 'Get accounts related to some venue by "venueID".' })
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async getByVenueID(@Param('id') id: string) {
        const data = await this.service.getByVenueID(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/checkPermission')
    @ApiOperation({ summary: 'Check requested permission for current account.' })
    @UseGuards(AuthGuard, BusinessAuthGuard, RoleGuard)
    async checkPermission(@Req() req) {
        const data = await this.service.checkPermission(req.account, req.body.roles);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Post()
    @ApiOperation({ summary: 'Add new account.' })
    @UseGuards(AuthGuard, RoleGuard)
    async create(@Body() createAccountDto: CreateAccountDto) {
        const newAccount = await this.service.create(createAccountDto);
        return {
            statusCode: 200,
            message: 'Account has been submitted successfully!',
            data: newAccount
        };
    }

    @Put(':id')
    @ApiOperation({ summary: 'Update some account by "accountID".' })
    @UseGuards(AuthGuard, RoleGuard)
    async update(@Param('id') id: string, @Body() updateAccountDto: UpdateAccountDto) {
        const updatedAccount = await this.service.update(id, updateAccountDto);
        return {
            statusCode: 200,
            message: 'Account has been updated successfully!',
            data: updatedAccount
        };
    }

    @Delete(':id')
    @ApiOperation({ summary: 'Delete some account by "accountID".' })
    @UseGuards(AuthGuard, RoleGuard)
    async delete(@Param('id') id: string) {
        const deletedAccount = await this.service.delete(id);
        return {
            statusCode: 200,
            message: 'Account has been deleted!',
            data: deletedAccount
        };
    }

    @Delete('/permanently/:id')
    @ApiOperation({ summary: 'Delete some account permanently by "accountID".' })
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async deletePermanently(@Param('id') id: string) {
        const deletedAccount = await this.service.deletePermanently(id);
        return {
            statusCode: 200,
            message: 'Account has been deleted permanently!',
            data: deletedAccount
        };
    }
}
