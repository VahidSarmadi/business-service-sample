import {
  CanActivate,
  ExecutionContext, ForbiddenException,
  Injectable,
  Logger,
} from '@nestjs/common';
import { Reflector } from "@nestjs/core";

@Injectable()
export class RoleGuard implements CanActivate {
  private readonly logger = new Logger(RoleGuard.name);
  constructor(
      private readonly reflector: Reflector
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    if (!roles) {
      return true; // no role is required, so allow the request to proceed
    }

    const request = context.switchToHttp().getRequest();
    const user = request.user;
    if (!user) {
      throw new ForbiddenException('You are not authorized to access this resource.');
    }

    // const hasRole = () => user.roles.some(role => roles.includes(role));
    const hasRole = () => roles.includes(user.role);
    if (!hasRole()) {
      throw new ForbiddenException('You are not permitted to access this resource.');
    }

    return true;
  }
}
