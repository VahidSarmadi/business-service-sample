import { Injectable } from '@nestjs/common';
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";
import { Account, AccountDocument } from "./schemas/account.schema";
import { CreateAccountDto } from "./dto/create.dto";
import { UpdateAccountDto } from "./dto/update.dto";

@Injectable()
export class AccountService {
    constructor(
        @InjectModel(Account.name) private readonly accountModel: Model<AccountDocument>,
    ) {}

    async getAll(): Promise<Account[]> {
        return await this.accountModel.find({ 'deleted': false }).exec();
    }

    async getOne(id: string): Promise<Account> {
        return await this.accountModel.findOne({'_id': id, 'deleted': false}).exec();
    }

    async getByUserID(id: string): Promise<Account[]> {
        return await this.accountModel.find({'userID': id, 'deleted': false}).exec();
    }

    async getByVenueID(id: string): Promise<Account[]> {
        return await this.accountModel.find({'venueID': id, 'deleted': false}).exec();
    }

    async checkPermission(account, role: string | [string]): Promise<Boolean> {
        const roles = Array.isArray(role) ? role : [role]; // force input to be an array

        const hasRole = () => roles.includes(account.role);
        return hasRole();
    }

    async create(createAccountDto: CreateAccountDto, accountID?: string): Promise<Account> {
        return await new this.accountModel({
            ...createAccountDto,
            createdBy: accountID || undefined
        }).save();
    }

    async update(id: string, updateAccountDto: UpdateAccountDto, accountID?: string): Promise<Account> {
        return await this.accountModel.findByIdAndUpdate(id, {
            ...updateAccountDto,
            updatedBy: accountID || undefined
        }).exec();
    }

    async delete(id: string, accountID?: string): Promise<Account> {
        return await this.accountModel.findByIdAndUpdate(id,{
            deleted: true,
            deletedAt: new Date(),
            updatedBy: accountID || undefined
        }).exec();
    }

    async deletePermanently(id: string): Promise<Account> {
        return await this.accountModel.findByIdAndDelete(id).exec();
    }
}
