import { Module } from '@nestjs/common';
import { AccountService } from './account.service';
import { AccountController } from './account.controller';
import { ConfigModule } from "@nestjs/config";
import { HttpModule } from "@nestjs/axios";
import {MongooseModule} from "@nestjs/mongoose";
import {Account, AccountSchema} from "./schemas/account.schema";

@Module({
  imports: [
    ConfigModule,
    HttpModule,
    MongooseModule.forFeature([{ name: Account.name, schema: AccountSchema }]),
  ],
  providers: [AccountService],
  controllers: [AccountController],
  exports:[AccountService]
})
export class AccountModule {}
