import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
// import mongoose from "mongoose";
// import { Role } from "../../role/schemas/role.schema";

enum Role {
    PrimaryAdmin = 'PrimaryAdmin',
    CoAdmin = 'CoAdmin'
}

export type AccountDocument = Account & Document;

@Schema({ timestamps: true })
export class Account {
    @Prop({ required: true })
    userID: string;

    @Prop({ required: true })
    venueID: string;

    // @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Role', required: true })
    // role: Role;

    @Prop({ required: true, type: String, enum: Role })
    role: string;

    @Prop()
    label?: string;

    @Prop()
    description?: string;

    @Prop()
    deletedAt?: Date;

    @Prop()
    createdBy?: string;

    @Prop()
    updatedBy?: string;

    @Prop({ default: false })
    deleted: boolean
}

export const AccountSchema = SchemaFactory.createForClass(Account);