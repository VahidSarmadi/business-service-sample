import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
  Logger,
  UnauthorizedException
} from '@nestjs/common';
import { HttpService } from "@nestjs/axios";
import { ConfigService } from "@nestjs/config";
import {InjectModel} from "@nestjs/mongoose";
import {Account, AccountDocument} from "./schemas/account.schema";
import {Model} from "mongoose";

@Injectable()
export class AuthGuard implements CanActivate {
  private readonly logger = new Logger(AuthGuard.name);
  constructor(
      private readonly httpService: HttpService,
      private configService: ConfigService
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const token = request?.headers?.authorization;
    if (!token) throw new UnauthorizedException()
    if (!token.startsWith('Bearer ')) throw new UnauthorizedException()

    let response = await this.httpService.axiosRef.get(
        `${this.configService.get('services.userManagement.baseUrl')}${this.configService.get('services.userManagement.endpoints.getID')}`,
        {headers: { Authorization: token}}
        );

    //todo: need user Role
    let userID = response?.data?.user_id;
    let userRole = response?.data?.user_role || 'User';
    if(userID) {
      request.user = {
        id: userID,
        role: userRole
      };
    }

    return true;
  }
}

@Injectable()
export class BusinessAuthGuard implements CanActivate {
  private readonly logger = new Logger(BusinessAuthGuard.name);
  constructor(
      @InjectModel(Account.name) private readonly accountModel: Model<AccountDocument>,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const accountID = request?.body?.accountID;
    if (!accountID) throw new UnauthorizedException();

    let account = await this.accountModel.find({'_id': accountID, 'deleted': false}).exec();

    console.log(account);

    if(!account) throw new UnauthorizedException();
    // if(account.userID !== request.user.id) throw new UnauthorizedException();

    request.account = account;
    return true;
  }
}