import { Module } from '@nestjs/common';
import { ProfileService } from './services/profile.service';
import { ProfileController } from './controllers/profile.controller';
import {ConfigModule} from "@nestjs/config";
import {HttpModule} from "@nestjs/axios";
import {MongooseModule} from "@nestjs/mongoose";
import {Profile, ProfileSchema} from "./schemas/profile.schema";
import {Place, PlacesSchema} from "./schemas/place.schema";
import {Kyc, KycSchema} from "./schemas/kyc.schema";
import {AccountModule} from "../account/account.module";
import {KycController} from "./controllers/kyc.controller";
import {KycService} from "./services/kyc.service";
import {ClaimRequestController} from "./controllers/claim-request.controller";
import {ClaimRequestService} from "./services/claim-request.service";
import {ClaimRequest, ClaimRequestSchema} from "./schemas/claim-request.schema";
import {VoucherService} from "../voucher/voucher.service";

@Module({
  imports: [
    ConfigModule,
    HttpModule,
    MongooseModule.forFeature([
      { name: Place.name, schema: PlacesSchema },
      { name: Profile.name, schema: ProfileSchema },
      { name: Kyc.name, schema: KycSchema },
      { name: ClaimRequest.name, schema: ClaimRequestSchema },
    ]),
    AccountModule
  ],
  providers: [ProfileService, KycService, ClaimRequestService],
  controllers: [ProfileController, KycController, ClaimRequestController],
  exports: [ProfileService]
})
export class ProfileModule {}
