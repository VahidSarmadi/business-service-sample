import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Profile, ProfileDocument } from "../schemas/profile.schema";
import {CreateKycDto} from "../dto/create-kyc.dto";
import {UpdateKycDto} from "../dto/update-kyc.dto";
import {ClaimRequest, ClaimRequestDocument, ClaimRequestStatus} from "../schemas/claim-request.schema";
import {KYCStatus} from "../schemas/kyc.schema";

@Injectable()
export class ClaimRequestService {

    constructor(
        @InjectModel(Profile.name) private readonly profileModel: Model<ProfileDocument>,
        @InjectModel(ClaimRequest.name) private readonly claimRequestModel: Model<ClaimRequestDocument>,
    ) {}

    async getAll(): Promise<ClaimRequest[]> {
        return await this.claimRequestModel.find({ 'deleted': false }).exec();
    }

    async getOne(id: string): Promise<ClaimRequest> {
        return await this.claimRequestModel.findOne({'_id': id, 'deleted': false}).exec();
    }

    async create(createKycDto: CreateKycDto, userID?: string): Promise<ClaimRequest> {
        const profile = await this.profileModel.findOne({'_id': createKycDto.profileId, 'deleted': false}).exec();
        if(!profile) throw new HttpException('Wrong profileID!', HttpStatus.BAD_REQUEST);

        const claimRequestOld = await this.claimRequestModel.findOne({'profileId': createKycDto.profileId,  'status': ClaimRequestStatus.Pending, 'deleted': false}).exec();
        if(!!claimRequestOld) throw new HttpException('This profileId already has a pending claim request!', HttpStatus.BAD_REQUEST);

        createKycDto.userDescription = createKycDto.description;

        return await new this.claimRequestModel({
            ...createKycDto,
            createdBy: userID || undefined
        }).save();
    }

    async approve(id: string, updateKycDto: UpdateKycDto, userID?: string): Promise<ClaimRequest> {
        const claimRequest = await this.claimRequestModel.findOne({'_id': id, 'deleted': false}).exec();
        if(!claimRequest) throw new HttpException('Wrong ClaimRequestID!', HttpStatus.BAD_REQUEST);

        const profile = await this.profileModel.findOne({'_id': claimRequest.profileId, 'deleted': false}).exec();
        if(!profile) throw new HttpException('Wrong ProfileID!', HttpStatus.BAD_REQUEST);
        profile.kycStatus = KYCStatus.Rejected;
        profile.active = false;
        await profile.save();
        //todo: send email and notification

        claimRequest.status = ClaimRequestStatus.Approved;
        claimRequest.adminDescription = updateKycDto.description;
        claimRequest.updatedBy = userID || undefined;
        return await claimRequest.save();
    }

    async reject(id: string, updateKycDto: UpdateKycDto, userID?: string): Promise<ClaimRequest> {
        const claimRequest = await this.claimRequestModel.findOne({'_id': id, 'deleted': false}).exec();
        if(!claimRequest) throw new HttpException('Wrong ClaimRequestID!', HttpStatus.BAD_REQUEST);

        claimRequest.status = ClaimRequestStatus.Rejected;
        claimRequest.adminDescription = updateKycDto.description;
        claimRequest.updatedBy = userID || undefined;
        return await claimRequest.save();
    }

    async delete(id: string, accountID?: string): Promise<ClaimRequest> {
        return await this.claimRequestModel.findByIdAndUpdate(id,{
            deleted: true,
            deletedAt: new Date(),
            updatedBy: accountID || undefined
        }, {new: true}).exec();
    }

    async deletePermanently(id: string): Promise<ClaimRequest> {
        return await this.claimRequestModel.findByIdAndDelete(id).exec();
    }
}
