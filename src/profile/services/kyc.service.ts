import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Profile, ProfileDocument } from "../schemas/profile.schema";
import {Kyc, KycDocument, KYCStatus} from "../schemas/kyc.schema";
import {CreateKycDto} from "../dto/create-kyc.dto";
import {UpdateKycDto} from "../dto/update-kyc.dto";

@Injectable()
export class KycService {

    constructor(
        @InjectModel(Profile.name) private readonly profileModel: Model<ProfileDocument>,
        @InjectModel(Kyc.name) private readonly kycModel: Model<KycDocument>,
    ) {}

    async getAll(): Promise<Kyc[]> {
        return await this.kycModel.find({ 'deleted': false }).exec();
    }

    async getOne(id: string): Promise<Kyc> {
        return await this.kycModel.findOne({'_id': id, 'deleted': false}).exec();
    }

    async create(createKycDto: CreateKycDto, userID?: string): Promise<Kyc> {
        const profile = await this.profileModel.findOne({'_id': createKycDto.profileId, 'deleted': false}).exec();
        if(!profile) throw new HttpException('Wrong profileID!', HttpStatus.BAD_REQUEST);

        const kycOld = await this.kycModel.findOne({'profileId': createKycDto.profileId,  'status': KYCStatus.Pending, 'deleted': false}).exec();
        if(!!kycOld) throw new HttpException('This profileId already has a pending kyc request!', HttpStatus.BAD_REQUEST);

        createKycDto.userDescription = createKycDto.description;

        let kyc = await new this.kycModel({
            ...createKycDto,
            createdBy: userID || undefined
        }).save();
        profile.kycStatus = KYCStatus.Pending;
        await profile.save();
        return kyc;
    }

    async approve(id: string, updateKycDto: UpdateKycDto, userID?: string): Promise<Kyc> {
        const kyc = await this.kycModel.findOne({'_id': id, 'deleted': false}).exec();
        if(!kyc) throw new HttpException('Wrong KycID!', HttpStatus.BAD_REQUEST);

        const profile = await this.profileModel.findOne({'_id': kyc.profileId, 'deleted': false}).exec();
        if(!profile) throw new HttpException('Wrong ProfileID!', HttpStatus.BAD_REQUEST);
        profile.kycStatus = KYCStatus.Approved;
        await profile.save();

        kyc.status = KYCStatus.Approved;
        kyc.adminDescription = updateKycDto.description;
        kyc.updatedBy = userID || undefined;
        return await kyc.save();
    }

    async reject(id: string, updateKycDto: UpdateKycDto, userID?: string): Promise<Kyc> {
        const kyc = await this.kycModel.findOne({'_id': id, 'deleted': false}).exec();
        if(!kyc) throw new HttpException('Wrong KycID!', HttpStatus.BAD_REQUEST);

        const profile = await this.profileModel.findOne({'_id': kyc.profileId, 'deleted': false}).exec();
        if(!profile) throw new HttpException('Wrong ProfileID!', HttpStatus.BAD_REQUEST);
        profile.kycStatus = KYCStatus.Rejected;
        await profile.save();

        kyc.status = KYCStatus.Rejected;
        kyc.adminDescription = updateKycDto.description;
        kyc.updatedBy = userID || undefined;
        return await kyc.save();
    }

    async delete(id: string, accountID?: string): Promise<Kyc> {
        return await this.kycModel.findByIdAndUpdate(id,{
            deleted: true,
            deletedAt: new Date(),
            updatedBy: accountID || undefined
        }, {new: true}).exec();
    }

    async deletePermanently(id: string): Promise<Kyc> {
        return await this.kycModel.findByIdAndDelete(id).exec();
    }
}
