import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import * as qrcode from 'qrcode';
import { Model } from "mongoose";
import { CreateProfileDto } from '../dto/create-profile.dto';
import { UpdateProfileDto } from '../dto/update-profile.dto';
import { Profile, ProfileDocument } from "../schemas/profile.schema";
import { Place, PlaceDocument } from "../schemas/place.schema";
import {ConfigService} from "@nestjs/config";
import axios from 'axios';
import {CreateProfileStep1Dto} from "../dto/create-profile-step1.dto";
import {AccountService} from "../../account/account.service";
import {CreateProfileStep2Dto} from "../dto/create-profile-step2.dto";
import {CreateProfileStep3Dto} from "../dto/create-profile-step3.dto";
import {CreateProfileStep4Dto} from "../dto/create-profile-step4.dto";

@Injectable()
export class ProfileService {
    private readonly API_KEY:string;

    constructor(
        @InjectModel(Profile.name) private readonly profileModel: Model<ProfileDocument>,
        @InjectModel(Place.name) private readonly placeModel: Model<PlaceDocument>,
        private readonly configService: ConfigService,
        private readonly accountService: AccountService
    ) {
        this.API_KEY = this.configService.get('GOOGLE_PLACES_API_KEY');
    }

    async getAll(): Promise<Profile[]> {
        // let place = await this.getPlaceData("ChIJg5n1oMY1vD8RWr4-LafOcf0");

        return await this.profileModel.find({ 'deleted': false, 'active': true }).exec();
    }

    async getOne(id: string): Promise<Profile> {
        // todo: uncomment active...
        return await this.profileModel.findOne({'_id': id, 'deleted': false/*, 'active': true*/}).exec();
    }

    async create(createProfileDto: CreateProfileDto, userID?: string): Promise<Profile> {
        //todo: fetch place data from google api
        //todo: create new account for this userID
        return await new this.profileModel({
            ...createProfileDto,
            createdBy: userID || undefined
        }).save();
    }

    async setup_step1(createProfileDto: CreateProfileStep1Dto, userID: string): Promise<object> {
        //todo: fetch place data from google api

        let {placeIsUnique, profileId} = await this.placeIdIsUnique(createProfileDto.placeID);
        if(!placeIsUnique) throw new HttpException({
            statusCode: HttpStatus.BAD_REQUEST,
            message: 'This Place Submitted Before!',
            data: {
                profileId: profileId
            },
        }, HttpStatus.BAD_REQUEST);

        // let place = await this.getPlaceData(createProfileDto.placeID);
        // let place = await this.getPlaceData("ChIJg5n1oMY1vD8RWr4-LafOcf0");
        // console.log(place);
        // createProfileDto.place = place;

        createProfileDto.userID = userID;

        let newProfile = await new this.profileModel({
            ...createProfileDto,
            createdBy: userID || undefined
        }).save();

        let newAccount = await this.accountService.create({
            userID: userID,
            venueID: newProfile._id,
            role: 'PrimaryAdmin'
        });

        return { accountID: newAccount['_id'], profileID: newProfile._id };
    }

    async setup_step2(createProfileDto: CreateProfileStep2Dto, userID?: string): Promise<Profile> {
        createProfileDto.setupLvl = "STEP2";
        let profile = await this.profileModel.findByIdAndUpdate(createProfileDto.profileID, {
            ...createProfileDto,
            updatedBy: userID || undefined
        }, {new: true}).exec();
        if(!profile) throw new HttpException('Wrong profileID!', HttpStatus.BAD_REQUEST);

        return profile;
    }

    async setup_step3(createProfileDto: CreateProfileStep3Dto, userID?: string): Promise<object> {

        createProfileDto.setupLvl = "STEP3";
        let profile = await this.profileModel.findByIdAndUpdate(createProfileDto.profileID, {
            ...createProfileDto,
            updatedBy: userID || undefined
        }, {new: true}).exec();
        if(!profile) throw new HttpException('Wrong profileID!', HttpStatus.BAD_REQUEST);

        return profile;
    }

    async setup_step4(createProfileDto: CreateProfileStep4Dto, userID?: string): Promise<object> {

        createProfileDto.setupLvl = "STEP4";
        let profile = await this.profileModel.findByIdAndUpdate(createProfileDto.profileID, {
            ...createProfileDto,
            updatedBy: userID || undefined
        }, {new: true}).exec();
        if(!profile) throw new HttpException('Wrong profileID!', HttpStatus.BAD_REQUEST);

        return profile;
    }

    async update(id: string, updateProfileDto: UpdateProfileDto, userID?: string): Promise<Profile> {
        return await this.profileModel.findByIdAndUpdate(id, {
            ...updateProfileDto,
            updatedBy: userID || undefined
        }, {new: true}).exec();
    }

    async delete(id: string): Promise<Profile> {
        return await this.profileModel.findByIdAndUpdate(id,{
            deleted: true,
            deletedAt: new Date()
        }, {new: true}).exec();
    }

    async deletePermanently(id: string): Promise<Profile> {
        return await this.profileModel.findByIdAndDelete(id).exec();
    }

    async getPlaceData(placeId: string): Promise<any> {
        console.log(placeId);
        console.log(this.API_KEY);
        try {
            const url = `https://maps.googleapis.com/maps/api/place/details/json?placeid=${placeId}&key=${this.API_KEY}`;
            const response = await axios.get(url);
            console.log(response);
            if (response.status === 200) {
                return response.data.result;
            } else {
                throw new Error('Error fetching place data from Google Places API');
            }
        } catch (e) {
            throw new Error('Connection error from Google Places API');
        }
    }

    async placeIdIsUnique(placeId: string): Promise<any> {
        let profile = await this.profileModel.findOne({'placeID': placeId, 'deleted': false, 'active': true}).exec();
        let placeIsUnique = !!!profile;
        return {placeIsUnique, profileId: profile?._id};
    }

    async generateQRCode(url: string): Promise<Buffer> {
        return await qrcode.toBuffer(url);
    }
}
