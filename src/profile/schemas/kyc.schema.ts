import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type KycDocument = Kyc & Document;

export enum KYCStatus {
    NotUploaded = "NotUploaded",
    Pending = "Pending",
    Approved = "Approved",
    Rejected = "Rejected",
    Expired = "Expired",
}

@Schema()
export class Kyc {
    @Prop({ required: true })
    profileId: string;

    @Prop({ required: true })
    identity: string;

    @Prop({ required: true })
    address: string;

    @Prop({ enum: KYCStatus, default: KYCStatus.Pending })
    status: string;

    @Prop()
    userDescription: string;

    @Prop()
    adminDescription: string;

    @Prop({
        required: true,
        default: new Date(Date.now() + 365 * 24 * 60 * 60 * 1000), // expires after one year by default
    })
    expiration: Date;

    @Prop()
    deletedAt?: Date;

    @Prop()
    createdBy?: string;

    @Prop()
    updatedBy?: string;

    @Prop({default: false})
    deleted: Boolean;
}

export const KycSchema = SchemaFactory.createForClass(Kyc);