import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ClaimRequestDocument = ClaimRequest & Document;

export enum ClaimRequestStatus {
    Pending = "Pending",
    Approved = "Approved",
    Rejected = "Rejected"
}

@Schema()
export class ClaimRequest {
    @Prop({ required: true })
    profileId: string;

    @Prop({ required: true })
    placeId: string;

    @Prop({ required: true })
    identity: string;

    @Prop({ required: true })
    address: string;

    @Prop({ enum: ClaimRequestStatus, default: ClaimRequestStatus.Pending })
    status: string;

    @Prop()
    userDescription: string;

    @Prop()
    adminDescription: string;

    @Prop()
    deletedAt?: Date;

    @Prop()
    createdBy?: string;

    @Prop()
    updatedBy?: string;

    @Prop({default: false})
    deleted: Boolean;
}

export const ClaimRequestSchema = SchemaFactory.createForClass(ClaimRequest);