import {Prop, raw, Schema, SchemaFactory} from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Place } from './place.schema';
import {KYCStatus} from "./kyc.schema";

enum SetupLvl {
    STEP1 = "STEP1",
    STEP2 = "STEP2",
    STEP3 = "STEP3",
    STEP4 = "STEP4",
    STEP5 = "STEP5"
}

export type ProfileDocument = Profile & mongoose.Document;

@Schema({timestamps: true})
export class Profile {
    @Prop({ required: true })
    userID: string;

    @Prop({ required: true })
    name: string;

    @Prop()
    description?: string;

    @Prop()
    websiteUrl?: string;

    @Prop({ type: [String] })
    phones: string[];

    @Prop({ required: true })
    country: string;

    @Prop({ required: true })
    city: string;

    @Prop({ required: true })
    postalCode: string;

    @Prop({ required: true })
    addressLine1: string;

    @Prop()
    addressLine2: string;

    @Prop({ required: true })
    placeID: string;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Place'/*, required: true*/ })
    place: Place;

    @Prop(raw({
        instagram: { type: String },
        telegram: { type: String },
        twitter: { type: String },
        linkedin: { type: String }
    }))
    social: Record<string, any>;

    @Prop({ type: [String] })
    logo: string[];

    @Prop({ type: [String] })
    cover: string[];

    @Prop()
    capacity: Number;

    @Prop()
    category: string;

    @Prop({ type: [
            {
                day: { type: String },
                openTime: { type: Number },
                closeTime: { type: Number },
            },
        ]})
    openHours: {
        day: string,
        // a number representing the opening time for that day, in minutes past midnight (e.g. 480 for 8:00 AM)
        openTime: number,
        closeTime: number,
    }[];

    @Prop()
    subcategories: string[];

    @Prop()
    atmosphere: string[];

    @Prop({default: false})
    onboardingDone: Boolean;

    @Prop({type: String, enum: SetupLvl, default: SetupLvl.STEP1})
    setupLvl: string;

    @Prop({ enum: KYCStatus, default: KYCStatus.NotUploaded })
    kycStatus: string;

    @Prop({default: true})
    active: Boolean;

    @Prop()
    deletedAt?: Date;

    @Prop()
    createdBy?: string;

    @Prop()
    updatedBy?: string;

    @Prop({default: false})
    deleted: Boolean;
}

export const ProfileSchema = SchemaFactory.createForClass(Profile);