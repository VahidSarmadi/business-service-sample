import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type PlaceDocument = Place & Document;

@Schema()
export class Place {
    @Prop()
    id: string;

    @Prop()
    name: string;

    @Prop()
    address: string;

    @Prop({ type: { lat: Number, lng: Number } })
    location: { lat: number, lng: number };

    @Prop()
    rating: number;

    @Prop({ type: [String] })
    types: string[];

    @Prop({ type: [{ photo_reference: String, width: Number, height: Number }] })
    photos: { photo_reference: string, width: number, height: number }[];
    
    @Prop()
    deletedAt?: Date;

    @Prop()
    createdBy?: string;

    @Prop()
    updatedBy?: string;

    @Prop({default: false})
    deleted: Boolean;
}

export const PlacesSchema = SchemaFactory.createForClass(Place);