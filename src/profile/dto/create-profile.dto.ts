import { BaseProfileDto } from "./base-profile.dto";
import {ApiProperty} from "@nestjs/swagger";
import {IsArray, IsNotEmpty, IsOptional, IsString} from "class-validator";

export class CreateProfileDto extends BaseProfileDto {
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    name: string;

    @ApiProperty()
    @IsOptional()
    @IsArray()
    subcategories?: string[];

    @ApiProperty()
    @IsOptional()
    @IsArray()
    atmosphere?: string[];
}