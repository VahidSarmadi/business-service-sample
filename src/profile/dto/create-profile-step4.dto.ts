import { BaseProfileDto } from "./base-profile.dto";
import {ApiProperty} from "@nestjs/swagger";
import {IsArray, IsMongoId, IsNotEmpty, IsOptional, IsString, IsNumber, ValidateNested} from "class-validator";
import {Type} from "class-transformer";

class OpenHourDto {
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    day: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    openTime: number;

    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    closeTime: number;
}

export class CreateProfileStep4Dto extends BaseProfileDto {
    @ApiProperty()
    @IsMongoId()
    @IsNotEmpty()
    profileID: string;

    @ApiProperty()
    @IsArray()
    @ValidateNested({ each: true })
    @Type(() => OpenHourDto)
    openHours: OpenHourDto[];

    setupLvl?: string;
}