import { BaseProfileDto } from "./base-profile.dto";
import {ApiProperty} from "@nestjs/swagger";
import {IsArray, IsMongoId, IsNotEmpty, IsOptional, IsString, IsNumber, ValidateNested} from "class-validator";
import {Type} from "class-transformer";
import {resolveSrv} from "dns";

export class CreateProfileStep3Dto extends BaseProfileDto {
    @ApiProperty()
    @IsMongoId()
    @IsNotEmpty()
    profileID: string;

    @ApiProperty()
    @IsArray()
    @IsNotEmpty()
    atmosphere: string[];

    setupLvl?: string;
}