import { BaseProfileDto } from "./base-profile.dto";
import {ApiProperty} from "@nestjs/swagger";
import {IsArray, IsMongoId, IsNotEmpty, IsOptional, IsString} from "class-validator";

export class CreateProfileStep2Dto extends BaseProfileDto {
    @ApiProperty()
    @IsMongoId()
    @IsNotEmpty()
    profileID: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    category: string;

    @ApiProperty()
    @IsArray()
    @IsNotEmpty()
    subcategories: string[];

    setupLvl?: string;
}