import {ApiProperty} from "@nestjs/swagger";
import {IsArray, IsMongoId, IsNotEmpty, IsOptional, IsString, IsNumber, ValidateNested} from "class-validator";

export class CreateKycDto {
    @ApiProperty()
    @IsMongoId()
    @IsNotEmpty()
    profileId: string;

    @ApiProperty()
    @IsNotEmpty()
    address: string;

    @ApiProperty()
    @IsNotEmpty()
    identity: string;

    @ApiProperty()
    @IsOptional()
    description?: string;

    userDescription?: string;
}