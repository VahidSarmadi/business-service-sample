import {ApiProperty} from "@nestjs/swagger";
import {IsArray, IsMongoId, IsNotEmpty, IsOptional, IsString, IsNumber, ValidateNested} from "class-validator";

export class UpdateKycDto {
    @ApiProperty()
    @IsOptional()
    description?: string;
}