import { BaseProfileDto } from "./base-profile.dto";
import {ApiProperty} from "@nestjs/swagger";
import {IsArray, IsNotEmpty, IsOptional, IsString} from "class-validator";

export class CreateProfileStep1Dto extends BaseProfileDto {
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    name: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    country: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    city: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    postalCode: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    addressLine1: string;

    @ApiProperty()
    @IsString()
    @IsOptional()
    addressLine2: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    placeID: string;

    @ApiProperty()
    @IsOptional()
    @IsString()
    description?: string;

    @ApiProperty()
    @IsOptional()
    @IsString()
    websiteUrl?: string;

    @ApiProperty()
    @IsOptional()
    @IsArray()
    phones?: string[];

    place?: object;
    userID: string;
}