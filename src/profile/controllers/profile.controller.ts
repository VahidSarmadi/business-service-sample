import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put, Req, UseGuards,
} from '@nestjs/common';
import { CreateProfileDto } from '../dto/create-profile.dto';
import { UpdateProfileDto } from '../dto/update-profile.dto';
import { ProfileService } from '../services/profile.service';
import {ApiOperation, ApiTags} from "@nestjs/swagger";
import {AuthGuard, BusinessAuthGuard} from "../../account/auth.guard";
import {RoleGuard} from "../../account/role.guard";
import {Roles} from "../../account/roles.decorator";
import {CreateProfileStep1Dto} from "../dto/create-profile-step1.dto";
import {CreateProfileStep2Dto} from "../dto/create-profile-step2.dto";
import {CreateProfileStep3Dto} from "../dto/create-profile-step3.dto";
import {CreateProfileStep4Dto} from "../dto/create-profile-step4.dto";

@Controller('profile')
@ApiTags('profile')
export class ProfileController {
    constructor(private readonly service: ProfileService) {}

    @Get()
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async index() {
        const data = await this.service.getAll();
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get(':id')
    // @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async find(@Param('id') id: string) {
        const data = await this.service.getOne(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Post()
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async create(@Body() createProfileDto: CreateProfileDto) {
        const newProfile = await this.service.create(createProfileDto);
        return {
            statusCode: 200,
            message: 'Profile has been submitted successfully!',
            data: newProfile
        };
    }

    @Post('/setup/step1')
    @UseGuards(AuthGuard, RoleGuard)
    async setup_step1(@Req() req, @Body() createProfileDto: CreateProfileStep1Dto) {
        const newProfile = await this.service.setup_step1(createProfileDto, req.user.id);
        return {
            statusCode: 200,
            message: 'Profile(step1) has been submitted successfully!',
            data: newProfile
        };
    }

    @Post('/setup/step2')
    @UseGuards(AuthGuard, RoleGuard)
    async setup_step2(@Body() createProfileDto: CreateProfileStep2Dto) {
        const newProfile = await this.service.setup_step2(createProfileDto);
        return {
            statusCode: 200,
            message: 'Profile(step2) has been submitted successfully!',
            data: newProfile
        };
    }

    @Post('/setup/step3')
    @UseGuards(AuthGuard, RoleGuard)
    async setup_step3(@Body() createProfileDto: CreateProfileStep3Dto) {
        const newProfile = await this.service.setup_step3(createProfileDto);
        return {
            statusCode: 200,
            message: 'Profile(step3) has been submitted successfully!',
            data: newProfile
        };
    }

    @Post('/setup/step4')
    @UseGuards(AuthGuard, RoleGuard)
    async setup_step4(@Body() createProfileDto: CreateProfileStep4Dto) {
        const newProfile = await this.service.setup_step4(createProfileDto);
        return {
            statusCode: 200,
            message: 'Profile(step4) has been submitted successfully!',
            data: newProfile
        };
    }

    // @Post('/generate-qr-code/:profileID')
    // @UseGuards(AuthGuard, BusinessAuthGuard, RoleGuard)
    // async generateQrCode(@Body() createProfileDto: CreateProfileStep4Dto) {
    //     const qrcode = await this.service.generateQRCode("test");
    //     return {
    //         statusCode: 200,
    //         message: 'QrCode generated successfully!',
    //         data: qrcode
    //     };
    // }

    @Put(':id')
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async update(@Param('id') id: string, @Body() updateProfileDto: UpdateProfileDto) {
        const updatedProfile = await this.service.update(id, updateProfileDto);
        return {
            statusCode: 200,
            message: 'Profile has been updated successfully!',
            data: updatedProfile
        };
    }

    @Delete(':id')
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async delete(@Param('id') id: string) {
        const deletedProfile = await this.service.delete(id);
        return {
            statusCode: 200,
            message: 'Profile has been deleted!',
            data: deletedProfile
        };
    }

    @Delete('/permanently/:id')
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async deletePermanently(@Param('id') id: string) {
        const deletedProfile = await this.service.deletePermanently(id);
        return {
            statusCode: 200,
            message: 'Profile has been deleted permanently!',
            data: deletedProfile
        };
    }
}
