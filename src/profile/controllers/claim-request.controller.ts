import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put, Req, UseGuards,
} from '@nestjs/common';
import {ApiOperation, ApiTags} from "@nestjs/swagger";
import {AuthGuard, BusinessAuthGuard} from "../../account/auth.guard";
import {RoleGuard} from "../../account/role.guard";
import {Roles} from "../../account/roles.decorator";
import {CreateKycDto} from "../dto/create-kyc.dto";
import {UpdateKycDto} from "../dto/update-kyc.dto";
import {ClaimRequestService} from "../services/claim-request.service";

@Controller('profile/claim-business')
@ApiTags('claim-business')
export class ClaimRequestController {
    constructor(private readonly service: ClaimRequestService) {}

    @Get('get')
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async index() {
        const data = await this.service.getAll();
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('get/:id')
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async find(@Param('id') id: string) {
        const data = await this.service.getOne(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Post('submit')
    @UseGuards(AuthGuard, RoleGuard)
    async create(@Body() createKycDto: CreateKycDto) {
        const newKyc = await this.service.create(createKycDto);
        return {
            statusCode: 200,
            message: 'Claim request has been submitted successfully!',
            data: newKyc
        };
    }

    @Post('/approve/:claimRequestID')
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async approve(@Param('claimRequestID') id: string, @Body() updateKycDto: UpdateKycDto) {
        const newClaimRequest = await this.service.approve(id, updateKycDto);
        return {
            statusCode: 200,
            message: 'Claim request approved!',
            data: newClaimRequest
        };
    }

    @Post('/reject/:claimRequestID')
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async reject(@Param('claimRequestID') id: string, @Body() updateKycDto: UpdateKycDto) {
        const newClaimRequest = await this.service.reject(id, updateKycDto);
        return {
            statusCode: 200,
            message: 'Claim request rejected!',
            data: newClaimRequest
        };
    }

    @Delete(':id')
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async delete(@Param('id') id: string) {
        const deletedClaimRequest = await this.service.delete(id);
        return {
            statusCode: 200,
            message: 'Claim request has been deleted!',
            data: deletedClaimRequest
        };
    }

    @Delete('/permanently/:id')
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async deletePermanently(@Param('id') id: string) {
        const deletedClaimRequest = await this.service.deletePermanently(id);
        return {
            statusCode: 200,
            message: 'Claim request has been deleted permanently!',
            data: deletedClaimRequest
        };
    }
}
