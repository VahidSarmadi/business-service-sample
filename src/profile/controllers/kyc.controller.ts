import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put, Req, UseGuards,
} from '@nestjs/common';
import { KycService } from '../services/kyc.service';
import {ApiOperation, ApiTags} from "@nestjs/swagger";
import {AuthGuard, BusinessAuthGuard} from "../../account/auth.guard";
import {RoleGuard} from "../../account/role.guard";
import {Roles} from "../../account/roles.decorator";
import {CreateKycDto} from "../dto/create-kyc.dto";
import {UpdateKycDto} from "../dto/update-kyc.dto";

@Controller('profile/kyc')
@ApiTags('kyc')
export class KycController {
    constructor(private readonly service: KycService) {}

    @Get('get')
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async index() {
        const data = await this.service.getAll();
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('get/:id')
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async find(@Param('id') id: string) {
        const data = await this.service.getOne(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Post('submit')
    @UseGuards(AuthGuard, RoleGuard)
    async create(@Body() createKycDto: CreateKycDto) {
        const newKyc = await this.service.create(createKycDto);
        return {
            statusCode: 200,
            message: 'Kyc has been submitted successfully!',
            data: newKyc
        };
    }

    @Post('/approve/:KycID')
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async approve(@Param('KycID') id: string, @Body() updateKycDto: UpdateKycDto) {
        const newKyc = await this.service.approve(id, updateKycDto);
        return {
            statusCode: 200,
            message: 'KYC approved!',
            data: newKyc
        };
    }

    @Post('/reject/:KycID')
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async reject(@Param('KycID') id: string, @Body() updateKycDto: UpdateKycDto) {
        const newKyc = await this.service.reject(id, updateKycDto);
        return {
            statusCode: 200,
            message: 'KYC rejected!',
            data: newKyc
        };
    }

    @Delete(':id')
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async delete(@Param('id') id: string) {
        const deletedKyc = await this.service.delete(id);
        return {
            statusCode: 200,
            message: 'KYC has been deleted!',
            data: deletedKyc
        };
    }

    @Delete('/permanently/:id')
    @UseGuards(AuthGuard, RoleGuard)
    // @Roles('Admin')
    async deletePermanently(@Param('id') id: string) {
        const deletedKyc = await this.service.deletePermanently(id);
        return {
            statusCode: 200,
            message: 'KYC has been deleted permanently!',
            data: deletedKyc
        };
    }
}
