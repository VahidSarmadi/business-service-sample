import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import mongoose from "mongoose";

export enum OrderStatus {
    Draft = "Draft",
    Checkout = "Checkout",
    Submit = "Submit",
    Payment = "Payment",
    Payed = "Payed",
    PendingFriend = "PendingFriend",
    ConvertedToVoucher = "ConvertedToVoucher",
    Pending = "Pending",
    Confirmed = "Confirmed",
    InProgress = "InProgress",
    Rejected = "Rejected",
    RejectedByFriend = "RejectedByFriend",
    Completed = "Completed",
    Delivered = "Delivered",
    Canceled = "Canceled",
    Archived = "Archived"
}

export enum OrderType {
    DeliverToTable = "Deliver to my table",
    Pickup = "I want to pickup",
    OrderForSomeoneElse = "Order for someone else"
}

export enum TipType {
    None = 0,
    TenPercent = 10,
    FifteenPercent = 15,
    EighteenPercent = 18,
    TwentyPercent = 20
}

export type OrderDocument = Order & Document;

@Schema({ timestamps: true })
export class Order {
    @Prop({
        type: String,
        // unique: true,
        required: true,
        default: Math.floor(1000000000 + Math.random() * 9000000000).toString(),
        index: true,
        immutable: true,
    })
    orderNumber: string;

    @Prop({ type: mongoose.Schema.Types.ObjectId, required: true, ref: "Profile" })
    venueID: string;

    @Prop()
    userID?: string;

    @Prop({ type: Object })
    guestInfo?: {
        name: string;
        mobile: string;
    }

    @Prop()
    tableNumber?: string;

    @Prop()
    note?: string;

    @Prop({ required: true, type: [Object] })
    items: {
        productID: string,
        product: object,
        quantity: number,
        price: string,
    }[];

    @Prop({ required: true })
    totalAmount: number;

    @Prop({ required: true })
    payableAmount: number;

    @Prop({ default: 0 })
    payedAmount: number;

    @Prop({ type: String, enum: OrderStatus, default: OrderStatus.Draft})
    status: string;

    @Prop({ type: String, enum: OrderType, required: true })
    orderType: string;

    @Prop({ type: Object })
    orderFor?: {
        userID: string;
        name: string;
    }

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: "Voucher" })
    voucher?: string;

    @Prop({ enum: TipType, default: TipType.None })
    tip: number;

    @Prop({ default: 0 })
    tipAmount: number;

    @Prop({ default: false })
    isGuest: boolean;

    @Prop({ default: true })
    active: boolean;

    @Prop()
    deletedAt?: Date;

    @Prop()
    createdBy?: string;

    @Prop()
    updatedBy?: string;

    @Prop({ default: false })
    deleted: boolean;
}

export const OrderSchema = SchemaFactory.createForClass(Order);