import {BadRequestException, HttpException, HttpStatus, Injectable, NotFoundException} from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Order, OrderDocument, OrderStatus, OrderType} from "./schemas/order.schema";
import {Model, Types} from "mongoose";
import {CreateOrderDto} from "./dto/create.dto";
import {UpdateOrderDto} from "./dto/update.dto";
import {ProductService} from "../product/product.service";
import {VoucherService} from "../voucher/voucher.service";
import {ProfileService} from "../profile/services/profile.service";

@Injectable()
export class OrderService {
    constructor(
        @InjectModel(Order.name) private readonly orderModel: Model<OrderDocument>,
        private readonly productService: ProductService,
        private readonly voucherService: VoucherService,
        private readonly profileService: ProfileService,
    ) {}

    async getAll(): Promise<Order[]> {
        return await this.orderModel.find({ 'deleted': false }).exec();
    }

    async getOne(id: string): Promise<Order> {
        return await this.orderModel.findOne({'_id': id, 'deleted': false}).exec();
    }

    async getByVenueID(id: string): Promise<Order[]> {
        return await this.orderModel.find({ 'venueID': id, 'deleted': false }).exec();
    }

    async getByVenueTable(venueID: string, tableNumber): Promise<Order> {
        return await this.orderModel.findOne({ 'venueID': venueID, 'tableNumber': tableNumber, 'active': true, 'deleted': false }).exec();
    }

    async getByUserID(id: string): Promise<Order[]> {
        return await this.orderModel.find({ 'userID': id, 'deleted': false }).exec();
    }

    async getLastActiveOrderByUserID(id: string): Promise<Order> {
        return await this.orderModel.findOne({ 'userID': id, 'status': { $in: ['Checkout', 'Submit'] }, 'deleted': false }).exec();
    }

    async create(createOrderDto: CreateOrderDto, userID: string): Promise<Order> {
        const { venueID, orderType, tableNumber, items, orderFor } = createOrderDto;

        const orderNumber = Math.floor(1000000000 + Math.random() * 9000000000).toString();

        const venue = await this.profileService.getOne(venueID);
        if(!venue) throw new NotFoundException(`Venue not found!`);

        if(tableNumber) {
            const _order = await this.getByVenueTable(venue['_id'], tableNumber);
            if(!!_order) throw new BadRequestException('This table has already an active order!')
        }
        // calculate total amount
        let totalAmount = 0;
        for (const item of items) {
            const product = await this.productService.getOne(item.productID);
            if(!product) throw new NotFoundException(`Product not found!`);
            if(product.venueID !== venueID) throw new NotFoundException(`This product does not belong to this venue!`);
            totalAmount += item.quantity * Number(product.price);
            item.product = product;
        }

        // create order document
        const order = new this.orderModel({
            orderNumber,
            venueID,
            userID,
            orderType,
            tableNumber,
            orderFor,
            items,
            totalAmount,
            payableAmount: totalAmount,
            status: OrderStatus.Checkout,
        });

        // save order to database
        await order.save();

        return order;
    }

    async create_guest(createOrderDto: CreateOrderDto): Promise<Order> {
        const { venueID, orderType, tableNumber, items, orderFor, guestInfo } = createOrderDto;

        if(!guestInfo.name || !guestInfo.mobile) throw new HttpException('Guest info required!', HttpStatus.BAD_REQUEST);
        const orderNumber = Math.floor(1000000000 + Math.random() * 9000000000).toString();

        const venue = await this.profileService.getOne(venueID);
        if(!venue) throw new NotFoundException(`Venue not found!`);

        if(tableNumber) {
            const _order = await this.getByVenueTable(venue['_id'], tableNumber);
            if(!!_order) throw new BadRequestException('This table has already an active order!')
        }

        // calculate total amount
        let totalAmount = 0;
        for (const item of items) {
            const product = await this.productService.getOne(item.productID);
            if(!product) throw new NotFoundException(`Product not found!`);
            if(product.venueID !== venueID) throw new NotFoundException(`This product does not belong to this venue!`);
            totalAmount += item.quantity * Number(product.price);
            item.product = product;
        }

        // create order document
        const order = new this.orderModel({
            orderNumber,
            venueID,
            guestInfo,
            orderType,
            tableNumber,
            orderFor,
            items,
            totalAmount,
            isGuest: true,
            payableAmount: totalAmount,
            status: OrderStatus.Checkout,
        });

        // save order to database
        await order.save();

        return order;
    }

    async submit(id: string, updateOrderDto: UpdateOrderDto, userID?: string): Promise<Order> {
        if (!Types.ObjectId.isValid(id)) {
            throw new HttpException('Invalid Order ID', HttpStatus.BAD_REQUEST);
        }

        const { note, tip, voucher } = updateOrderDto;

        let order = await this.orderModel.findOne({'_id': id, 'deleted': false}).exec();
        if(!order) throw new HttpException('Wrong OrderID!', HttpStatus.NOT_FOUND);

        if(!order.isGuest) {
            if(
                !(order.status === OrderStatus.Checkout ||
                    order.status === OrderStatus.Submit)
            ) throw new HttpException(`Order with number ${order.orderNumber} cannot be submit in its current state`, HttpStatus.BAD_REQUEST);
        }


        let finalPayableAmount = order.totalAmount;

        // apply voucher discount if voucher code is provided
        let discountAmount = 0;
        if (voucher) {
            const voucherDoc = await this.voucherService.getOne(voucher);
            if(!voucherDoc) {
                throw new HttpException('Invalid Voucher ID', HttpStatus.BAD_REQUEST);
            }
            let recheckVoucher = false;
            for (const item of order.items) {
                if(item.productID === voucherDoc.product["_id"].toString()) {
                    recheckVoucher = true;
                    discountAmount = Number(voucherDoc.product["price"]);
                    break;
                }
            }
            if(!recheckVoucher) throw new HttpException('Invalid Voucher ID', HttpStatus.BAD_REQUEST);
            voucherDoc.used = true;
            voucherDoc.usedAt = new Date();
            // @ts-ignore
            await voucherDoc.save();
        }

        // calculate final payable amount
        finalPayableAmount = finalPayableAmount - discountAmount;

        // apply tip
        const tipPercentage = tip / 100;
        const tipAmount = Math.floor(order.totalAmount * tipPercentage * 10) / 10;
        finalPayableAmount = finalPayableAmount + tipAmount;



        // update order document
        order.tip = tip;
        order.tipAmount = tipAmount;
        order.note = note;
        order.voucher = voucher;
        order.payableAmount = finalPayableAmount;
        if(order.isGuest) order.status = OrderStatus.Pending;
        else order.status = OrderStatus.Submit;
        order.updatedBy = userID || undefined;
        return await order.save();

    }

    async acceptByVenue(id: string, userID?: string): Promise<Order> {
        // todo: send notification
        const order = await this.orderModel.findOne({ _id: id, deleted: false }).exec();
        if (!order) {
            throw new NotFoundException(`Order not found`);
        }
        if (order.status !== OrderStatus.Pending) {
            throw new BadRequestException(`Order with number ${order.orderNumber} cannot be accepted in its current state`);
        }
        order.status = OrderStatus.Confirmed;
        order.updatedBy = userID || undefined;
        // order.note = note;
        return await order.save();
    }

    async rejectByVenue(id: string, userID?: string): Promise<Order> {
        // todo: send notification
        const order = await this.orderModel.findOne({ _id: id, deleted: false }).exec();
        if (!order) {
            throw new NotFoundException(`Order not found`);
        }
        if (order.status !== OrderStatus.Pending) {
            throw new BadRequestException(`Order with number ${order.orderNumber} cannot be rejected in its current state`);
        }
        order.status = OrderStatus.Rejected;
        order.updatedBy = userID || undefined;
        // order.note = note;
        return await order.save();
    }

    async acceptByFriendAsOrder(id: string, userID: string): Promise<Order> {
        // todo: send notification
        const order = await this.orderModel.findOne({ _id: id, deleted: false }).exec();
        if (!order) {
            throw new NotFoundException(`Order not found`);
        }
        if (order.status !== OrderStatus.PendingFriend) {
            throw new BadRequestException(`Order with number ${order.orderNumber} cannot be accepted in its current state`);
        }

        if (order.orderFor.userID !== userID) {
            throw new BadRequestException(`You dont have access to change this order!`);
        }
        order.status = OrderStatus.Pending;
        order.updatedBy = userID || undefined;
        // order.note = note;
        return await order.save();
    }

    async acceptByFriendAsVoucher(id: string, userID: string): Promise<Order> {
        // todo: send notification
        const order = await this.orderModel.findOne({ _id: id, deleted: false }).exec();
        if (!order) {
            throw new NotFoundException(`Order not found`);
        }
        if (order.status !== OrderStatus.PendingFriend) {
            throw new BadRequestException(`Order with number ${order.orderNumber} cannot be accepted in its current state`);
        }

        if (order.orderFor.userID !== userID) {
            throw new BadRequestException(`You dont have access to change this order!`);
        }

        try {
            for(let i = 0; i < order.items.length; i++) {
                for(let j = 0; j < order.items[i].quantity; j++) {
                    const newVoucher = await this.voucherService.create({
                        product: order.items[i].productID,
                        venue: order.venueID,
                        userID: order.orderFor.userID,
                    });
                }
            }
        } catch (e) {
            console.error(e);
            throw new BadRequestException(`Can not create voucher!`);
        }

        order.status = OrderStatus.ConvertedToVoucher;
        order.updatedBy = userID || undefined;
        return await order.save();
    }

    async rejectByFriend(id: string, userID?: string): Promise<Order> {
        // todo: send notification
        const order = await this.orderModel.findOne({ _id: id, deleted: false }).exec();
        if (!order) {
            throw new NotFoundException(`Order not found`);
        }
        if (order.status !== OrderStatus.PendingFriend) {
            throw new BadRequestException(`Order with number ${order.orderNumber} cannot be rejected in its current state`);
        }
        if (order.orderFor.userID !== userID) {
            throw new BadRequestException(`You dont have access to change this order!`);
        }
        try {
            for(let i = 0; i < order.items.length; i++) {
                for(let j = 0; j < order.items[i].quantity; j++) {
                    const newVoucher = await this.voucherService.create({
                        product: order.items[i].productID,
                        venue: order.venueID,
                        userID: order.userID,
                    });
                }
            }
        } catch (e) {
            console.error(e);
            throw new BadRequestException(`Can not create voucher!`);
        }
        order.status = OrderStatus.RejectedByFriend;
        order.updatedBy = userID || undefined;
        // order.note = note;
        return await order.save();
    }

    async inProgress(id: string, userID?: string): Promise<Order> {
        // todo: send notification
        const order = await this.orderModel.findOne({ _id: id, deleted: false }).exec();
        if (!order) {
            throw new NotFoundException(`Order not found`);
        }
        if (order.status !== OrderStatus.Confirmed) {
            throw new BadRequestException(`Order with number ${order.orderNumber} cannot be "inProgress" in its current state`);
        }
        order.status = OrderStatus.InProgress;
        order.updatedBy = userID || undefined;
        // order.note = note;
        return await order.save();
    }

    async cancel(id: string, userID?: string): Promise<Order> {
        // todo: send notification
        const order = await this.orderModel.findOne({ _id: id, deleted: false }).exec();
        if (!order) {
            throw new NotFoundException(`Order not found`);
        }
        if (
            order.status === OrderStatus.Completed ||
            order.status === OrderStatus.RejectedByFriend ||
            order.status === OrderStatus.Rejected ||
            order.status === OrderStatus.ConvertedToVoucher ||
            order.status === OrderStatus.Archived
        ) {
            throw new BadRequestException(`Order with number ${order.orderNumber} cannot be canceled in its current state`);
        }
        order.status = OrderStatus.Canceled;
        order.updatedBy = userID || undefined;
        // order.note = note;
        return await order.save();
    }

    async complete(id: string, userID?: string): Promise<Order> {
        // todo: send notification
        const order = await this.orderModel.findOne({ _id: id, deleted: false }).exec();
        if (!order) {
            throw new NotFoundException(`Order not found`);
        }
        if (order.status !== OrderStatus.InProgress) {
            throw new BadRequestException(`Order with number ${order.orderNumber} cannot be canceled in its current state`);
        }
        order.status = OrderStatus.Completed;
        order.updatedBy = userID || undefined;
        // order.note = note;
        return await order.save();
    }

    async deliver(id: string, userID?: string): Promise<Order> {
        // todo: send notification
        const order = await this.orderModel.findOne({ _id: id, deleted: false }).exec();
        if (!order) {
            throw new NotFoundException(`Order not found`);
        }
        if (order.status !== OrderStatus.Completed) {
            throw new BadRequestException(`Order with number ${order.orderNumber} cannot be deliver in its current state`);
        }
        order.status = OrderStatus.Delivered;
        order.active = false;
        order.updatedBy = userID || undefined;
        // order.note = note;
        return await order.save();
    }

    async archive(id: string, userID?: string): Promise<Order> {
        // todo: send notification
        const order = await this.orderModel.findOne({ _id: id, deleted: false }).exec();
        if (!order) {
            throw new NotFoundException(`Order not found`);
        }
        if (order.status !== OrderStatus.Delivered) {
            throw new BadRequestException(`Order with number ${order.orderNumber} cannot be canceled in its current state`);
        }
        order.status = OrderStatus.Archived;
        order.updatedBy = userID || undefined;
        // order.note = note;
        return await order.save();
    }

    async confirmPayment(id: string, amount: number): Promise<Order> {
        let order = await this.orderModel.findOne({'_id': id, 'deleted': false}).exec();

        order.payedAmount = amount;
        if(order.orderType === OrderType.OrderForSomeoneElse) {
            order.status = OrderStatus.PendingFriend;
        } else {
            if(order.status === OrderStatus.Submit) order.status = OrderStatus.Pending;
        }

        return await order.save();

    }

    async update(id: string, updateOrderDto: UpdateOrderDto, userID?: string): Promise<Order> {
        return await this.orderModel.findByIdAndUpdate(id, {
            ...updateOrderDto,
            updatedBy: userID || undefined
        }).exec();
    }

    async delete(id: string): Promise<Order> {
        return await this.orderModel.findByIdAndUpdate(id,{
            deleted: true,
            deletedAt: new Date()
        }).exec();
    }

    async deletePermanently(id: string): Promise<Order> {
        return await this.orderModel.findByIdAndDelete(id).exec();
    }

    async updateOrderStatus(orderID: string, status: OrderStatus): Promise<Order> {
        const order = await this.orderModel.findOne({'_id': orderID, 'deleted': false}).exec();
        if (!order) {
            throw new NotFoundException(`Order not found!`);
        }
        order.status = status;
        return await order.save();
    }

}
