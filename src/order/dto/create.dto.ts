import {
    IsArray,
    IsEnum,
    IsMongoId,
    IsNotEmpty,
    IsNumber,
    IsOptional,
    IsString,
    ValidateIf,
    ValidateNested
} from 'class-validator';
import {ApiProperty} from '@nestjs/swagger';
import {Type} from 'class-transformer';
import {OrderType, TipType} from '../schemas/order.schema';

class OrderItemDto {
    @ApiProperty({ description: 'Product ID' })
    @IsNotEmpty()
    @IsString()
    @IsMongoId()
    readonly productID: string;

    @ApiProperty({ description: 'Quantity of the product' })
    @IsNotEmpty()
    @IsNumber()
    readonly quantity: number;

    @ApiProperty({ description: 'Price of the product' })
    @IsNotEmpty()
    @IsString()
    readonly price: string;

    product?: object;
}

class OrderForDto {
    @ApiProperty({ description: 'User ID' })
    @IsNotEmpty()
    @IsString()
    readonly userID: string;

    @ApiProperty({ description: 'Name of the user' })
    @IsNotEmpty()
    @IsString()
    readonly name: string;
}

class GuestDto {
    @ApiProperty({ description: 'Mobile number' })
    @IsNotEmpty()
    @IsString()
    readonly mobile: string;

    @ApiProperty({ description: 'Name of the user' })
    @IsNotEmpty()
    @IsString()
    readonly name: string;
}

export class CreateOrderDto {
    @ApiProperty({ description: 'Venue ID' })
    @IsNotEmpty()
    @IsString()
    @IsMongoId()
    readonly venueID: string;

    @ApiProperty({ description: 'Table Number' })
    @IsNotEmpty()
    @ValidateIf((dto) => dto.orderType === OrderType.DeliverToTable)
    @IsString()
    readonly tableNumber?: string;

    @ApiProperty({ description: 'Order note' })
    @IsOptional()
    @IsString()
    readonly note?: string;

    @ApiProperty({ description: 'List of order items', type: [OrderItemDto] })
    @IsNotEmpty()
    @IsArray()
    @ValidateNested({ each: true })
    @Type(() => OrderItemDto)
    readonly items: OrderItemDto[];

    @ApiProperty({ description: 'Total amount of the order' })
    @IsOptional()
    @IsNumber()
    readonly totalAmount?: number;

    // @ApiProperty({ description: 'Payable amount of the order' })
    // @IsNotEmpty()
    // @IsNumber()
    // readonly payableAmount: number;

    // @ApiProperty({ description: 'Status of the order', enum: OrderStatus })
    // @IsOptional()
    // @IsEnum(OrderStatus)
    // readonly status?: OrderStatus;

    @ApiProperty({ description: 'Type of the order', enum: OrderType })
    @IsNotEmpty()
    @IsEnum(OrderType)
    readonly orderType: OrderType;

    @ApiProperty({ description: 'Order for (if applicable)', type: OrderForDto })
    @IsNotEmpty()
    @ValidateIf((dto) => dto.orderType === OrderType.OrderForSomeoneElse)
    @ValidateNested()
    @Type(() => OrderForDto)
    readonly orderFor?: OrderForDto;

    @ApiProperty({ description: 'Voucher ID' })
    @IsOptional()
    @IsString()
    @IsMongoId()
    readonly voucher?: string;

    @ApiProperty({ description: 'Tip (if applicable)', enum: TipType })
    @IsOptional()
    @IsEnum(TipType)
    readonly tip?: TipType = TipType.None;

    @ApiProperty({ description: 'Guest info (if applicable)' })
    @IsOptional()
    @ValidateNested()
    @Type(() => GuestDto)
    readonly guestInfo?: GuestDto;
}