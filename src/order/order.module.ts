import {forwardRef, Module} from '@nestjs/common';
import { OrderService } from './order.service';
import { OrderController } from './order.controller';
import {ConfigModule} from "@nestjs/config";
import {HttpModule} from "@nestjs/axios";
import {MongooseModule} from "@nestjs/mongoose";
import {Order, OrderSchema} from "./schemas/order.schema";
import {ProductModule} from "../product/product.module";
import {VoucherModule} from "../voucher/voucher.module";
import {ProfileModule} from "../profile/profile.module";
import {TransactionModule} from "../transactions/transaction.module";

@Module({
  imports: [
    ConfigModule,
    HttpModule,
    MongooseModule.forFeature([{ name: Order.name, schema: OrderSchema }]),
    ProductModule,
    ProfileModule,
    VoucherModule,
    TransactionModule
  ],
  providers: [OrderService],
  controllers: [OrderController],
  exports: [OrderService]
})
export class OrderModule {}
