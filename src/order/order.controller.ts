import {
    BadRequestException,
    Body,
    Controller,
    Delete,
    Get,
    NotFoundException,
    Param,
    Post,
    Put,
    Req,
    UseGuards
} from '@nestjs/common';
import {OrderService} from "./order.service";
import {CreateOrderDto} from "./dto/create.dto";
import {UpdateOrderDto} from "./dto/update.dto";
import {AuthGuard} from "../account/auth.guard";
import {RoleGuard} from "../account/role.guard";
import {ApiTags} from "@nestjs/swagger";
import {TransactionService} from "../transactions/transaction.service";
import {OrderStatus} from "./schemas/order.schema";

@Controller('order')
@ApiTags('order')
export class OrderController {
    constructor(
        private readonly service: OrderService,
        private readonly transactionService: TransactionService
    ) {}

    @Get()
    @UseGuards(AuthGuard, RoleGuard)
    async getAll() {
        const data = await this.service.getAll();
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get(':id')
    @UseGuards(AuthGuard, RoleGuard)
    async getOne(@Param('id') id: string) {
        const data = await this.service.getOne(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/venue/:venueID')
    @UseGuards(AuthGuard, RoleGuard)
    async getByVenueID(@Param('venueID') id: string) {
        const data = await this.service.getByVenueID(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/guest/:venueID/:tableNumber')
    // @UseGuards(AuthGuard, RoleGuard)
    async getByVenueTable(@Param('venueID') venueID: string, @Param('tableNumber') tableNumber: string) {
        const data = await this.service.getByVenueTable(venueID, tableNumber);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/user/:userID')
    @UseGuards(AuthGuard, RoleGuard)
    async getByUserID(@Param('userID') id: string) {
        const data = await this.service.getByUserID(id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/current/user')
    @UseGuards(AuthGuard, RoleGuard)
    async getByToken(@Req() req) {
        const data = await this.service.getByUserID(req.user.id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Get('/current/user/active-order')
    @UseGuards(AuthGuard, RoleGuard)
    async getLastActiveOrder(@Req() req) {
        const data = await this.service.getLastActiveOrderByUserID(req.user.id);
        return {
            statusCode: 200,
            message: 'Done!',
            data: data
        };
    }

    @Post()
    @UseGuards(AuthGuard, RoleGuard)
    async create(@Req() req, @Body() createOrderDto: CreateOrderDto) {
        const newOrder = await this.service.create(createOrderDto, req.user.id);
        return {
            statusCode: 200,
            message: 'Order has been created successfully!',
            data: newOrder
        };
    }

    @Post('/guest')
    // @UseGuards(AuthGuard, RoleGuard)
    async create_guest(@Req() req, @Body() createOrderDto: CreateOrderDto) {
        const newOrder = await this.service.create_guest(createOrderDto);
        return {
            statusCode: 200,
            message: 'Order has been created successfully!',
            data: newOrder
        };
    }

    @Post('/:orderID/submit')
    // @UseGuards(AuthGuard, RoleGuard)
    async submitOrder(@Req() req, @Param('orderID') id: string, @Body() updateOrderDto: UpdateOrderDto) {
        const order = await this.service.submit(id, updateOrderDto);
        return {
            statusCode: 200,
            message: 'Order has been submitted successfully!',
            data: order
        };
    }

    @Post('/:orderID/venue-accept')
    @UseGuards(AuthGuard, RoleGuard)
    async acceptOrder(@Req() req, @Param('orderID') id: string) {
        const order = await this.service.acceptByVenue(id, req.user.id);
        return {
            statusCode: 200,
            message: 'Order has been accepted successfully!',
            data: order
        };
    }

    @Post('/:orderID/venue-reject')
    @UseGuards(AuthGuard, RoleGuard)
    async rejectOrder(@Req() req, @Param('orderID') id: string) {
        const order = await this.service.rejectByVenue(id, req.user.id);
        return {
            statusCode: 200,
            message: 'Order has been rejected successfully!',
            data: order
        };
    }

    @Post('/:orderID/friend-accept-order')
    @UseGuards(AuthGuard, RoleGuard)
    async acceptOrderAsOrder_friend(@Req() req, @Param('orderID') id: string) {
        const order = await this.service.acceptByFriendAsOrder(id, req.user.id);
        return {
            statusCode: 200,
            message: 'Order has been accepted successfully!',
            data: order
        };
    }

    @Post('/:orderID/friend-accept-voucher')
    @UseGuards(AuthGuard, RoleGuard)
    async acceptOrderAsVoucher_friend(@Req() req, @Param('orderID') id: string) {
        const order = await this.service.acceptByFriendAsVoucher(id, req.user.id);
        return {
            statusCode: 200,
            message: 'Order has been accepted as voucher!',
            data: order
        };
    }

    @Post('/:orderID/friend-reject')
    @UseGuards(AuthGuard, RoleGuard)
    async rejectOrder_friend(@Req() req, @Param('orderID') id: string) {
        const order = await this.service.rejectByFriend(id, req.user.id);
        return {
            statusCode: 200,
            message: 'Order has been rejected successfully!',
            data: order
        };
    }

    @Post('/:orderID/in-progress')
    @UseGuards(AuthGuard, RoleGuard)
    async inProgress(@Req() req, @Param('orderID') id: string) {
        const order = await this.service.inProgress(id, req.user.id);
        return {
            statusCode: 200,
            message: 'Order state changed to "inProgress"!',
            data: order
        };
    }

    @Post('/:orderID/cancel')
    @UseGuards(AuthGuard, RoleGuard)
    async cancel(@Req() req, @Param('orderID') id: string) {
        const order = await this.service.cancel(id, req.user.id);
        return {
            statusCode: 200,
            message: 'Order has been canceled!',
            data: order
        };
    }

    @Post('/:orderID/cancel')
    @UseGuards(AuthGuard, RoleGuard)
    async complete(@Req() req, @Param('orderID') id: string) {
        const order = await this.service.complete(id, req.user.id);
        return {
            statusCode: 200,
            message: 'Order has been completed successfully!',
            data: order
        };
    }

    @Post('/:orderID/archive')
    @UseGuards(AuthGuard, RoleGuard)
    async archive(@Req() req, @Param('orderID') id: string) {
        const order = await this.service.archive(id, req.user.id);
        return {
            statusCode: 200,
            message: 'Order has been archived successfully!',
            data: order
        };
    }

    @Put(':id')
    @UseGuards(AuthGuard, RoleGuard)
    async update(@Param('id') id: string, @Body() updateOrderDto: UpdateOrderDto) {
        const updatedOrder = await this.service.update(id, updateOrderDto);
        return {
            statusCode: 200,
            message: 'Order has been updated successfully!',
            data: updatedOrder
        };
    }

    @Delete(':id')
    @UseGuards(AuthGuard, RoleGuard)
    async delete(@Param('id') id: string) {
        const deletedOrder = await this.service.delete(id);
        return {
            statusCode: 200,
            message: 'Order has been deleted!',
            data: deletedOrder
        };
    }

    @Delete('/permanently/:id')
    @UseGuards(AuthGuard, RoleGuard)
    async deletePermanently(@Param('id') id: string) {
        const deletedOrder = await this.service.deletePermanently(id);
        return {
            statusCode: 200,
            message: 'Order has been deleted permanently!',
            data: deletedOrder
        };
    }
}
