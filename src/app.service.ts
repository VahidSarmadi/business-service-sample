import {Inject, Injectable} from '@nestjs/common';
import {applicationConfig} from "./config";
import {ConfigType} from "@nestjs/config";

@Injectable()
export class AppService {
  constructor(
      @Inject(applicationConfig.KEY)
      private appConfig: ConfigType<typeof applicationConfig>,
  ) {}

  getHello(): string {
    return `Hello from "Business" service...`;
  }
}